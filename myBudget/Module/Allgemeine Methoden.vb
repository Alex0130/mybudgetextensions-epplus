﻿Imports System
Imports System.Collections
Imports System.Data
Imports System.IO
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Windows.Forms
Imports System.Data.SqlClient

Imports TenTec.Windows.iGridLib

Imports Microsoft.Office.Interop
Imports OfficeOpenXml       '!ab added
Imports OfficeOpenXml.Style '!ab added

Module Allgemeine_Methoden

#Region "Berechtigungen"
    Function GotRights(txtTransaktion As String, ID As Integer, qryPNummer As String, dt As System.Data.DataTable) As String
        Console.WriteLine("< Function GotRights")

        Dim i As Integer
        GotRights = "none"

        If InStr(UCase(qryPNummer), "ADMIN") > 0 Then 'Admin darf alles
            Return "write"
            Exit Function
        End If

        Try
            If dt.Rows.Count = 0 Then
                Return "none"
            End If

            Dim foundRows() As Data.DataRow
            dt.CaseSensitive = False 'Groß und Kleinschreibung ignorieren
            foundRows = dt.Select("txtTransaktion = '" & txtTransaktion & "' and qryPNummer = '" & qryPNummer & "' and intID = " & ID & "")
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                If UCase(foundRows(0).Item("ReadWrite")) = "WRITE" Then
                    Return "write"
                Else
                    Return "read"
                End If
            Else
                Return "none"
            End If

        Catch ex As Exception
            Return "none"
        End Try
    End Function
#End Region

#Region "SQL Statement - SLAVE from Module 'Datenbank'"
    Function CreateSqlStatement(FixStatement As String, listSql() As tableSqlStatement, listGj() As Integer) As String
        Dim I As Long, J As Long
        Dim GotOnlyStatement As Boolean
        Dim FinalStatement As String

        If IsNothing(listGj) Then
            ReDim listGj(0)
            listGj(0) = Year(Now)
        End If

        Try
            If Not IsNothing(listSql) Then

                GotOnlyStatement = False
                For I = 0 To UBound(listSql) 'ODER Statements erzeugen
                    If listSql(I).Nur_Ohne = "Oder" Then
                        GotOnlyStatement = True
                        listSql(I).SqlStatement = listSql(I).SqlStatement & FixStatement & " and " & listSql(I).TblFeldname & " = '" & listSql(I).Kriterium & "'"
                        For J = 0 To UBound(listSql) 'Bestehendes "Oder" Statements um "OHNE" oder "NUR" erweitern
                            If listSql(J).Nur_Ohne = "Ohne" Then
                                listSql(I).SqlStatement = listSql(I).SqlStatement & " and not " & listSql(J).TblFeldname & " = '" & listSql(J).Kriterium & "'"
                            ElseIf listSql(J).Nur_Ohne = "Nur" Then
                                listSql(I).SqlStatement = listSql(I).SqlStatement & " and " & listSql(J).TblFeldname & " = '" & listSql(J).Kriterium & "'"
                            End If
                        Next J
                        For J = LBound(listGj) To UBound(listGj) 'Statement um GJ Einschränkung erweitern
                            If J = LBound(listGj) Then
                                FinalStatement = listSql(I).SqlStatement & " and qryGeschäftsjahr = " & listGj(J)
                            Else
                                FinalStatement = FinalStatement & " or " & listSql(I).SqlStatement & " and qryGeschäftsjahr = " & listGj(J)
                            End If
                        Next J
                        listSql(I).SqlStatement = FinalStatement
                    End If
                Next I

                If GotOnlyStatement = True Then 'Mit Sql Statement, mit Oder-Verbindung: Oder Ausdrücke verbinden
                    Dim countEntry As Integer = 0
                    For I = 0 To UBound(listSql)
                        If listSql(I).Nur_Ohne = "Oder" Then
                            If countEntry = 0 Then 'Erstes "Oder" Statement
                                CreateSqlStatement = " where " & listSql(I).SqlStatement
                                countEntry = countEntry + 1
                            Else
                                CreateSqlStatement = CreateSqlStatement & " or " & listSql(I).SqlStatement
                                countEntry = countEntry + 1
                            End If
                        End If
                    Next I
                    Return CreateSqlStatement
                Else 'Mit Sql Statement, aber ohne Oder-Verbindung: FixStatement um "OHNE" und "NUR" Kriterien erweitern
                    CreateSqlStatement = FixStatement
                    For I = 0 To UBound(listSql) 'Kriterien
                        If listSql(I).Nur_Ohne = "Ohne" Then
                            CreateSqlStatement = CreateSqlStatement & " and not " & listSql(I).TblFeldname & " = '" & listSql(I).Kriterium & "'"
                        ElseIf listSql(I).Nur_Ohne = "Nur" Then
                            CreateSqlStatement = CreateSqlStatement & " and " & listSql(I).TblFeldname & " = '" & listSql(I).Kriterium & "'"
                        End If
                    Next I
                    For J = 0 To UBound(listGj) 'GJ
                        If J = 0 Then
                            FinalStatement = CreateSqlStatement & " and qryGeschäftsjahr = " & listGj(J)
                        Else
                            FinalStatement = FinalStatement & " or " & CreateSqlStatement & " and qryGeschäftsjahr = " & listGj(J)
                        End If
                    Next J
                    CreateSqlStatement = " where " & FinalStatement
                    Return CreateSqlStatement
                End If

            Else 'Ohne SQL Statement
                For J = LBound(listGj) To UBound(listGj)
                    If J = LBound(listGj) Then
                        FinalStatement = FixStatement & " and qryGeschäftsjahr = " & listGj(J)
                    Else
                        FinalStatement = FinalStatement & " or " & FixStatement & " and qryGeschäftsjahr = " & listGj(J)
                    End If
                Next J
                CreateSqlStatement = " where " & FinalStatement
                Return CreateSqlStatement
            End If
        Catch
            CreateSqlStatement = " where " & FixStatement
            Return CreateSqlStatement
        End Try
    End Function

    Function SqlWhereFieldEquals(txtTransaktion As String, qryPNummer As String, dtAuth As System.Data.DataTable) As String
        Dim i As Integer
        Dim statement As String
        Dim boolGotRights As Boolean = False

        If InStr(UCase(qryPNummer), "ADMIN") > 0 Or LCase(GotRights("AL", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
            Return "(" & txtTransaktion & " <> '')"
            Exit Function
        End If

        Try
            If dtAuth.Rows.Count > 0 Then
                Dim foundRows() As Data.DataRow
                dtAuth.CaseSensitive = False 'Groß und Kleinschreibung ignorieren
                foundRows = dtAuth.Select("txtTransaktion = '" & UCase(txtTransaktion) & "' and qryPNummer = '" & UCase(qryPNummer) & "'")
                If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                    For i = 0 To foundRows.Count - 1
                        statement = statement & txtTransaktion & " = " & foundRows(i).Item("intID") & " or "
                        boolGotRights = True
                    Next
                Else
                    boolGotRights = False
                End If
            End If

            If boolGotRights = True Then
                Return "(" & Left(statement, Len(statement) - 4) & ")" 'Letzte 4 Zeichen abschneiden
            Else
                Return "(" & txtTransaktion & "= 'ERROR')" 'LEER = gibt keine Treffer
            End If

        Catch ex As Exception
            Return "(" & txtTransaktion & "= 'ERROR')" 'LEER = gibt keine Treffer
        End Try
    End Function
#End Region

#Region "Compare Mode"
    Function VergleichPlanZuIst(listPlan As System.Data.DataTable, listIst As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("< Function VergleichPlanZuIst")

        '1. Plan Liste entpivotisieren (Jeder Monat eine eigene Zeile)
        '2. Ist Zeilen der Datentabelle hinzufügen
        '3. Gesamttabelle nach Projekt, Arbeitspaket, Jahr, Monat, Entwurfstyp und Höhe der Kosten sortieren

        Dim i As Integer
        Dim listData As System.Data.DataTable

        '--------

        If IsNothing(listPlan) AndAlso IsNothing(listIst) Then
            MsgBox("Keine Daten vorhanden!", vbCritical, "Fehler")
            Exit Function
        End If

        '1. Plan Daten entpivotisieren
        listData = DataTable_Entpivotisieren(listPlan, False)

        '2. Ist Daten hinzufügen
        If Not IsNothing(listData) Then
            listData.Merge(listIst, False)
        ElseIf Not IsNothing(listIst) Then
            listData = listIst
        End If

        '3. Daten sortieren nach: Projekt, Arbeitspaket, Jahr, Monat, Entwurfstyp, Kosten
        If Not IsNothing(listData) Then
            If listPlan.Rows(0).Item("qryEntwurfstyp") = "Plan" OrElse listPlan.Rows(0).Item("qryEntwurfstyp") = "Soll" Then
                listData.DefaultView.Sort = "PROJEKT_ID asc, qryArbeitspaket asc, qryGeschäftsjahr asc, lngsort asc, qryEntwurfstyp desc, CostsPerYear desc"
            Else
                listData.DefaultView.Sort = "PROJEKT_ID asc, qryArbeitspaket asc, qryGeschäftsjahr asc, lngsort asc, qryEntwurfstyp asc, CostsPerYear desc"
            End If

            listData = listData.DefaultView.ToTable
        End If

        Return listData
    End Function

    Function DataTable_Entpivotisieren(listData As System.Data.DataTable, CostsPerMonth As Boolean) As System.Data.DataTable 'CostsPerMonth: Rechnet die Gesamtkosten pro Monat aus und setzte die Menge auf 1
        Console.WriteLine("< Function DataTable_Entpivotisieren")

        Dim i As Integer, j As Integer, z As Integer
        Dim EntPivot As System.Data.DataTable

        If Not listData.Rows.Count > 0 Then Exit Function

        EntPivot = listData.Clone() 'DataTable Struktur auf neue Gesamttabelle übertragen

        Try
            progress = 100
            For i = 0 To listData.Rows.Count - 1 'Geht alle Plan Daten durch
                If progress = i Then
                    Form_Edit.ToolStripStatusOperation.Text = "Daten entpivotisieren: " & i & "/" & listData.Rows.Count - 1
                    Form_Edit.StatusStrip.Refresh()
                    progress = progress + 100
                End If

                For j = 1 To 12 'geht alle Monate durch
                    Dim curMonth As String = ConvertIntegerToDoubleMonth(j).dblMonat

                    If Not listData.Rows(i)(curMonth) = 0 Then
                        EntPivot.ImportRow(listData.Rows(i)) 'Kopiert die komplette Zeile
                        Dim curRow As Integer = EntPivot.Rows.Count - 1

                        EntPivot.Rows(curRow)("lngSort") = j 'Monat im Nr. Feld speichern
                        EntPivot.Rows(curRow)("CostsPerYear") = EntPivot.Rows(curRow)(curMonth) * EntPivot.Rows(curRow)("curKosten_pro_Einheit")

                        If CostsPerMonth = True Then
                            EntPivot.Rows(curRow)(curMonth) = EntPivot.Rows(curRow)(curMonth) * EntPivot.Rows(curRow)("curKosten_pro_Einheit")
                            EntPivot.Rows(curRow)("curKosten_pro_Einheit") = 1
                            EntPivot.Rows(curRow)("AmountPerYear") = 1
                        Else
                            EntPivot.Rows(curRow)("AmountPerYear") = EntPivot.Rows(curRow)(curMonth)
                        End If


                        For z = 1 To 12 'Leert alle Monate außer den aktuellen Monat
                            If Not z = j Then
                                EntPivot.Rows(curRow)(ConvertIntegerToDoubleMonth(z).dblMonat) = 0
                            End If
                        Next z

                    End If
                Next j
            Next

            Return EntPivot
        Catch ex As Exception
            MsgBox("pbl_DataEntpivotisieren: " & ex.Message.ToString)
        End Try
    End Function
#End Region

#Region "Convert Methoden"
#Region "Convert Integer to Month and Month to Integer"
    'Convert Monat (Dbl to Int / Int to Dbl)

    Function ConvertIntegerToDoubleMonth(Monat As Integer) As tableMonat
        Dim strMonat As tableMonat
        strMonat.intMonat = Monat
        If Monat = 1 Then
            strMonat.dblMonat = "dblJanuar"
            strMonat.Monat = "Januar"
        ElseIf Monat = 2 Then
            strMonat.dblMonat = "dblFebruar"
            strMonat.Monat = "Februar"
        ElseIf Monat = 3 Then
            strMonat.dblMonat = "dblMärz"
            strMonat.Monat = "März"
        ElseIf Monat = 4 Then
            strMonat.dblMonat = "dblApril"
            strMonat.Monat = "April"
        ElseIf Monat = 5 Then
            strMonat.dblMonat = "dblMai"
            strMonat.Monat = "Mai"
        ElseIf Monat = 6 Then
            strMonat.dblMonat = "dblJuni"
            strMonat.Monat = "Juni"
        ElseIf Monat = 7 Then
            strMonat.dblMonat = "dblJuli"
            strMonat.Monat = "Juli"
        ElseIf Monat = 8 Then
            strMonat.dblMonat = "dblAugust"
            strMonat.Monat = "August"
        ElseIf Monat = 9 Then
            strMonat.dblMonat = "dblSeptember"
            strMonat.Monat = "September"
        ElseIf Monat = 10 Then
            strMonat.dblMonat = "dblOktober"
            strMonat.Monat = "Oktober"
        ElseIf Monat = 11 Then
            strMonat.dblMonat = "dblNovember"
            strMonat.Monat = "November"
        ElseIf Monat = 12 Then
            strMonat.dblMonat = "dblDezember"
            strMonat.Monat = "Dezember"
        End If
        Return strMonat
    End Function

    Function ConvertDoubleToIntegerMonth(Monat As String) As tableMonat
        Dim strMonat As tableMonat
        strMonat.dblMonat = Monat
        If Monat = "dblJanuar" Then
            strMonat.intMonat = 1
            strMonat.Monat = "Januar"
        ElseIf Monat = "dblFebruar" Then
            strMonat.intMonat = 2
            strMonat.Monat = "Februar"
        ElseIf Monat = "dblMärz" Then
            strMonat.intMonat = 3
            strMonat.Monat = "März"
        ElseIf Monat = "dblApril" Then
            strMonat.intMonat = 4
            strMonat.Monat = "April"
        ElseIf Monat = "dblMai" Then
            strMonat.intMonat = 5
            strMonat.Monat = "Mai"
        ElseIf Monat = "dblJuni" Then
            strMonat.intMonat = 6
            strMonat.Monat = "Juni"
        ElseIf Monat = "dblJuli" Then
            strMonat.intMonat = 7
            strMonat.Monat = "Juli"
        ElseIf Monat = "dblAugust" Then
            strMonat.intMonat = 8
            strMonat.Monat = "August"
        ElseIf Monat = "dblSeptember" Then
            strMonat.intMonat = 9
            strMonat.Monat = "September"
        ElseIf Monat = "dblOktober" Then
            strMonat.intMonat = 10
            strMonat.Monat = "Oktober"
        ElseIf Monat = "dblNovember" Then
            strMonat.intMonat = 11
            strMonat.Monat = "November"
        ElseIf Monat = "dblDezember" Then
            strMonat.intMonat = 12
            strMonat.Monat = "Dezember"
        End If
        Return strMonat
    End Function
#End Region


#Region "Convert String zu Array or Array to String"
    Function ConvertIntegerArrayToString(Array() As Integer) As String
        Dim i As Long
        Dim Text As String
        If Not IsNothing(Array) Then
            For i = LBound(Array) To UBound(Array)
                If i = LBound(Array) Then
                    Text = Array(i)
                Else
                    Text = Text & ";" & Array(i)
                End If
            Next
        End If

        Return Text
    End Function

    Function ConvertStringToIntegerArray(Text As String) As Integer()
        Dim i As Long
        Dim IntArray() As Integer
        Try
            Dim StrArray() As String = Text.Split(New Char() {";"c})
            ReDim IntArray(UBound(StrArray))
            For i = LBound(StrArray) To UBound(StrArray)
                IntArray(i) = CInt(StrArray(i))
            Next
        Catch
            ReDim IntArray(0)
            IntArray(0) = Year(Now)
        End Try

        Return IntArray
    End Function

    'Kovertierung von VB.DBNUll to Nothing 
    Public Function DBStringToString(item As Object) As String  '!ab added
        If TypeOf item Is String Then
            Return item
        Else
            Return Nothing
        End If
    End Function
#End Region
#End Region

#Region "Grid to Excel"
    Sub iGridToExcel(igrid As TenTec.Windows.iGridLib.iGrid)
        Try
            If igrid.Rows.Count = 0 OrElse igrid.Cols.Count = 0 Then Exit Sub
            Dim i As Integer, j As Integer

#Region "iGrid in Datatable convertieren - only visible rows and cols"
            Dim dt As New System.Data.DataTable

            Dim ColOrder As Integer
            For i = 0 To igrid.Cols.Count - 1 'Define Cols
                ColOrder = i
                For j = 0 To igrid.Cols.Count - 1 'Define Cols
                    If igrid.Cols(j).Order = ColOrder Then
                        If igrid.Cols(j).Visible = True Then
                            dt.Columns.Add(New DataColumn() With {.ColumnName = igrid.Cols(j).Key, .Caption = igrid.Cols(j).Text, .DataType = GetType(String), .DefaultValue = ""})
                        End If
                    End If
                Next
            Next

            Dim RowCount As Integer = -1
            For i = 0 To igrid.Rows.Count - 1
                Dim row = igrid.Rows(i)

                If row.Visible = True Then
                    dt.Rows.Add()
                    RowCount = RowCount + 1

                    Select Case row.Type
                        Case iGRowType.AutoGroupRow
                            dt.Rows(RowCount).Item(0) = row.RowTextCell.Value
                        Case iGRowType.ManualGroupRow
                            dt.Rows(RowCount).Item(0) = row.RowTextCell.Value
                        Case iGRowType.Normal
                            For j = 0 To igrid.Cols.Count - 1
                                If igrid.Cols(j).Visible = True Then
                                    dt.Rows(RowCount).Item(igrid.Cols(j).Key) = igrid.Rows(i).Cells(j).Value
                                End If
                            Next
                    End Select
                End If
            Next
#End Region

#Region "DataTable in String Format convertieren"
            Dim ArrList(dt.Rows.Count, dt.Columns.Count - 1) As String
            'Überschriften
            For i = 0 To dt.Columns.Count - 1
                ArrList(0, i) = dt.Columns(i).Caption
            Next

            For i = 0 To dt.Rows.Count - 1
                For j = 0 To dt.Columns.Count - 1
                    ArrList(i + 1, j) = dt.Rows(i)(j).ToString
                Next
            Next
#End Region

#Region "Daten in Excel schreiben"
            'EPPLUS Initaisierung !ab added
            Dim pck As New ExcelPackage
            Dim worksheet As ExcelWorksheet = pck.Workbook.Worksheets.Add("Mappe1")

            TableToExcelWorksheet(dt, worksheet)
            worksheet.Cells.AutoFitColumns()
            'Workbook als temporärer File abspeichern
            Dim tempExcelFilename = newTempExcelFileName()
            pck.SaveAs(New FileInfo(tempExcelFileName))

            'Temporäre Datei für Anzeige in Excel laden     '!ab            
            CloseCurrentExcelApp() '!ab
            exApp = New Excel.Application
            exApp.Visible = False
            exApp.Workbooks.Open(New FileInfo(tempExcelFileName).FullName())
            exApp.ScreenUpdating = True
            exApp.Calculation = XlCalculation.xlCalculationAutomatic
            exApp.WindowState = Excel.XlWindowState.xlMaximized
            exApp.Visible = True
#End Region

        Catch ex As Exception
            MsgBox("iGridToExcel:  " & ex.Message.ToString)
        End Try
    End Sub

    Public Sub TableToExcelWorksheet(dt As DataTable, sheet As ExcelWorksheet)
        Dim checkInteger As Integer
        Dim checkSingle As Single
        Dim checkDouble As Double
        Dim checkDecimal As Decimal

        'Column heading
        For i = 0 To dt.Columns.Count - 1
            sheet.Cells(1, i + 1).Value = dt.Columns(i).Caption
        Next

        'Data Columns -> worksheet
        For i = 0 To dt.Rows.Count - 1
            For j = 0 To dt.Columns.Count - 1
                If IsDBNull(dt.Rows(i).Item(j)) Then
                    sheet.Cells(i + 2, j + 1).Value = String.Empty
                ElseIf Integer.TryParse(dt.Rows(i).Item(j), checkInteger) Then
                    sheet.Cells(i + 2, j + 1).Value = CType(dt.Rows(i).Item(j), Integer)
                ElseIf Single.TryParse(dt.Rows(i).Item(j), checkSingle) Then
                    sheet.Cells(i + 2, j + 1).Value = CType(dt.Rows(i).Item(j), Single)
                ElseIf Double.TryParse(dt.Rows(i).Item(j), checkDouble) Then
                    sheet.Cells(i + 2, j + 1).Value = CType(dt.Rows(i).Item(j), Double)
                ElseIf Decimal.TryParse(dt.Rows(i).Item(j), checkDecimal) Then
                    sheet.Cells(i + 2, j + 1).Value = CType(dt.Rows(i).Item(j), Decimal)
                Else
                    sheet.Cells(i + 2, j + 1).Value = dt.Rows(i).Item(j)
                End If
            Next
        Next
    End Sub

    Public Function NewtempExcelFileName()      '!ab added
        Dim time As DateTime = DateTime.Now
        'Verzeichnis für temporäre excel files 
        Dim tempDir As String = My.Computer.FileSystem.SpecialDirectories.Temp
        Dim format As String = "dd.MM.yyyy_HH.mm.ss"
        Dim fn = tempDir & "\\myBudget" + time.ToString(format) + ".xslx"
        Return fn
    End Function
    Public Sub CloseCurrentExcelApp()
        On Error Resume Next
        exApp.Workbooks(1).Close(SaveChanges:=False)
        exWB.Close(False)
        exApp.Quit()
    End Sub

    Public Sub DeleteTemporaryExcelFiles()  '!ab
        On Error Resume Next
        Dim tempDir As String = My.Computer.FileSystem.SpecialDirectories.Temp
        Dim dir As DirectoryInfo = New DirectoryInfo(tempDir)
        For Each item As FileInfo In dir.EnumerateFiles("myBudget*.xslx")
            item.Delete()
        Next
    End Sub
#End Region

#Region "Check myBudget Version"
    Function GotRightmyBudgetVersion() As Boolean
        Try
            Dim dt As New System.Data.DataTable
            dt = Get_tblmyBudetVersion()

            If dt.Rows.Count = 0 Then
                MsgBox("Versionsprüfung fehlgeschlagen. System wird beendet.", vbCritical, "Fehler")
                Return False
            End If

            Dim oAssembly As System.Reflection.AssemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName
            Dim MyVersion = oAssembly.Version.ToString()
            Dim NeedVersion = dt.Rows(0).Item("NeededVersion")
            Dim CurVersion = dt.Rows(0).Item("CurrentVersion")

            If AllowmyBudgetVersion(NeedVersion, MyVersion) = False Then
                MsgBox("Diese Version von 'myBudget' ist von der Nutzung ausgeschlossen." & vbCrLf & "Bitte die neue Version herunterladen und installieren." & vbCrLf & "Alle Datein sind hier zu finden: Z:\E\EM\Alle\myBudget", vbCritical, "Fehler")
                Return False
            End If

            If AllowmyBudgetVersion(CurVersion, MyVersion) = False Then
                MsgBox("Es ist eine neue Version von 'myBudget' verfügbar." & vbCrLf & "Die Installation der neuen Version ist optional." & vbCrLf & "Alle Datein sind hier zu finden: Z:\E\EM\Alle\myBudget", vbInformation, "Info")
            End If

            If Not IsDBNull(dt.Rows(0).Item("InfoText")) Then
                If Not String.IsNullOrEmpty(dt.Rows(0).Item("InfoText")) Then
                    MsgBox(dt.Rows(0).Item("InfoText").ToString, vbInformation, "Information")
                End If
            End If

            Return True


        Catch ex As Exception
            MsgBox("GotRightmyBudgetVersion: " & ex.Message.ToString)
        End Try
    End Function

    Function AllowmyBudgetVersion(ref As String, my As String) As Boolean
        Dim cRef As String
        Dim cMy As String
        Try
            Do Until Len(ref) = 0
                If ref.IndexOf(".") < 0 Then
                    cRef = ref
                    ref = ref.Remove(0, Len(ref)) 'Ende
                Else
                    cRef = ref.Substring(0, ref.IndexOf("."))
                    ref = ref.Remove(0, Len(cRef) + 1)
                End If

                If my.IndexOf(".") < 0 Then
                    cMy = my
                    my = my.Remove(0, Len(my)) 'Ende
                Else
                    cMy = my.Substring(0, my.IndexOf("."))
                    my = my.Remove(0, Len(cMy) + 1)
                End If


                If CInt(cRef) > CInt(cMy) Then
                    Return False
                ElseIf CInt(cRef) < CInt(cMy) Then
                    Return True
                End If
            Loop
        Catch ex As Exception
            MsgBox("AllowmyBudgetVersion: " & ex.Message.ToString)
        End Try
        Return True
    End Function
#End Region

#Region "SAP Verbindung ändern"
    Public Function Set_SAP_Connection(System As String) As System.Data.DataTable
        Dim dt As New System.Data.DataTable
        Try
            dt.Columns.Add(New DataColumn() With {.ColumnName = "ApplicationServer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "systemnumber", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "system", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "MessageServer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Rows.Add()

            SAP_Logout() 'ggf. bestehende Verbindung trennen

            If UCase(System) = "P01" OrElse String.IsNullOrEmpty(System) Then
                dt.Rows(0).Item("ApplicationServer") = "sapp01.ibd.porsche.de"
                dt.Rows(0).Item("systemnumber") = "10"
                dt.Rows(0).Item("system") = "P01"
                dt.Rows(0).Item("MessageServer") = "sapp01.ibd.porsche.de"
            ElseIf UCase(System) = "K01" Then
                dt.Rows(0).Item("ApplicationServer") = "sapk01.ibd.porsche.de"
                dt.Rows(0).Item("systemnumber") = "20"
                dt.Rows(0).Item("system") = "K01"
                dt.Rows(0).Item("MessageServer") = "sapk01.ibd.porsche.de"
            End If
            Form_Edit.MenuSapSystem.Text = "SAP System: " & dt.Rows(0).Item("system").ToString

            Return dt

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Function
#End Region

#Region "Version Freeze"
    Public Sub FreezeVersion(ID_AP As Integer, Version As Integer, Entwurfstyp As String, Freigabe As Boolean, VersionsErklärung As String)

        Try
            Dim Index_version As Integer

            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            If MsgBox("Wollen Sie die aktuelle Version des Arbeitspaketes freigeben und damit abschließen?", MsgBoxStyle.OkCancel, "Freigabe") = vbOK Then
                Try
                    Update_VersionArbeitspaket(ID_AP, Version, Entwurfstyp, Freigabe, VersionsErklärung)
                    Form_Edit.ToolStripStatusOperation.Text = "Version erfolgreich freigegeben"
                    Form_Edit.StatusStrip.Refresh()
                Catch ex As Exception
                    MsgBox("FreezeVersion - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
            Else
                Exit Sub
            End If
            Try
                Get_VersionArbeitspaket(True, pbl_IdAp, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp)
                Index_version = Array.FindIndex(pbl_VersionAp, Function(f) f.lngVersion = pbl_lngVersion)
                pbl_lngVersion = pbl_VersionAp(ViewCombo.View_ComboboxVersion(Form_Edit.ComboVersion, Index_version, pbl_VersionAp)).lngVersion

                Form_Edit.iGrid_EnableEditFunctions(True, True, "") 'ggf. Berechtigungen neu laden
            Catch ex As Exception
                MsgBox(ex.Message.ToString, vbCritical, "Fehler")
            End Try

        Catch ex As Exception
            MsgBox("FreezeVersion - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Outlook Mail"
    Public Function startOutlook(ByVal toVal As String, ByVal subjectVal As String, ByVal bodyVal As String)
        Console.WriteLine("< Function startOutlook")
        Try
            'Return a reference to the MAPI layer

            Dim ol As New Outlook.Application()

            Dim ns As Outlook.NameSpace

            Dim fdMail As Outlook.MAPIFolder


            ns = ol.GetNamespace("MAPI")


            'Logs on the user

            'Profile: This is a string value that indicates what MAPI profile to use for logging on. Leave blank if using the currently logged on user, or set to an empty string ("") if you wish to use the default Outlook Profile. 

            'Password: The password for the indicated profile. Leave blank if using the currently logged on user, or set to an empty string ("") if you wish to use the default Outlook Profile password. 

            'ShowDialog: Set to True to display the Outlook Profile dialog box. 

            'NewSession: Set to True to start a new session. Set to False to use the current session. 

            ns.Logon(, , True, True)


            'create a new MailItem object

            Dim newMail As Outlook.MailItem


            'gets defaultfolder for my Outlook Outbox

            fdMail = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderOutbox)


            'assign values to the newMail MailItem

            newMail = fdMail.Items.Add(Outlook.OlItemType.olMailItem)

            newMail.Subject = subjectVal

            newMail.Body = bodyVal

            newMail.To = toVal

            newMail.SaveSentMessageFolder = fdMail



            'adds it to the draft box

            'newMail.Save()


            'adds it to the outbox

            newMail.Send()
        Catch ex As Exception
            MsgBox("startOutlook: " & ex.Message.ToString)
        End Try
    End Function
#End Region

#Region "EKant Erstellung"
    Public Function CreateEkant()
        Try
            Form_Edit.ToolStripStatusOperation.Text = "E-Kostenantrag wird erstellt!..."
            Form_Edit.StatusStrip.Refresh()


            Dim i As Integer, j As Integer
            Dim boolIvpMatch As Boolean
            Dim dtOld As New System.Data.DataTable
            Dim dtEkant As New System.Data.DataTable

#Region "Datentabelle bereits stellen und neue Struktur anlegen"
            dtOld.Merge(ConvertGridtoDataTable(Form_Edit.iGrid), False)
            dtEkant = dtOld.Clone
#End Region

            If dtOld.Rows.Count = 0 Then Exit Function

#Region "EKant Prüfungen"
            Dim dt As New System.Data.DataTable
            dt = Get_tblEkant(True)

            For i = 0 To dtOld.Rows.Count - 1
                If IsNothing(dtOld.Rows(i).Item("txtKst_Lst").ToString) OrElse String.IsNullOrEmpty(dtOld.Rows(i).Item("txtKst_Lst").ToString) Then
                    MsgBox("Fehlendes Sachkonto" & vbCrLf & vbCrLf & "Arbeitspaket: " & dtOld.Rows(i).Item("txtArbeitspaket").ToString & vbCrLf & "Jahr: " & dtOld.Rows(i).Item("qryGeschäftsjahr").ToString & vbCrLf & "Zeile: " & i + 1 & " - Nr. " & dtOld.Rows(i).Item("lngSort").ToString & vbCrLf & vbCrLf & "EKant Erstellung wird abgebrochen.", vbCritical, "Fehler")
                    Exit Function
                ElseIf dtOld.Rows(i).Item("qryKostenart").ToString = "Arbeitskosten" OrElse dtOld.Rows(i).Item("qryKostenart").ToString = "Leistungsarten" Then
                    If String.IsNullOrEmpty(dtOld.Rows(i).Item("txtBedarfsnummer").ToString) Then
                        MsgBox("Fehlende Kostenstelle" & vbCrLf & vbCrLf & "Arbeitspaket:  " & dtOld.Rows(i).Item("txtArbeitspaket").ToString & vbCrLf & "Jahr: " & dtOld.Rows(i).Item("qryGeschäftsjahr").ToString & vbCrLf & "Zeile: " & i + 1 & " - Nr. " & dtOld.Rows(i).Item("lngSort").ToString & vbCrLf & vbCrLf & "EKant Erstellung wird abgebrochen.", vbCritical, "Fehler")
                        Exit Function
                    End If
                    If String.IsNullOrEmpty(dtOld.Rows(i).Item("curKosten_pro_Einheit").ToString) Then
                        MsgBox("Fehlende 'Kosten pro Einheit'" & vbCrLf & vbCrLf & "Arbeitspaket: " & dtOld.Rows(i).Item("txtArbeitspaket").ToString & vbCrLf & "Jahr: " & dtOld.Rows(i).Item("qryGeschäftsjahr").ToString & vbCrLf & "Zeile: " & i + 1 & " - Nr. " & dtOld.Rows(i).Item("lngSort").ToString & vbCrLf & vbCrLf & "EKant Erstellung wird abgebrochen.", vbCritical, "Fehler")
                        Exit Function
                    End If

                    boolIvpMatch = False
                    Dim dtIvp As DataRow()
                    'dtIvp = dt.Select("Kostenstelle = '" & dtOld.Rows(i).Item("txtBedarfsnummer").ToString & "'")
                    dtIvp = dt.Select("Kostenstelle = '" & dtOld.Rows(i).Item("txtBedarfsnummer").ToString & "' And intGJ = " & dtOld.Rows(i).Item("qryGeschäftsjahr")) '!ab + GJ
                    If Not IsNothing(dtIvp) AndAlso dtIvp.Count > 0 Then
                        For j = 0 To dtIvp.Count - 1
                            If dtIvp(j).Item("Std_Satz").ToString = dtOld.Rows(i).Item("curKosten_pro_Einheit").ToString Then
                                boolIvpMatch = True
                                Exit For
                            End If
                        Next
                        If boolIvpMatch = False Then
                            MsgBox("Falscher IVP!" & vbCrLf & vbCrLf & "Arbeitspaket: " & dtOld.Rows(i).Item("txtArbeitspaket").ToString & vbCrLf & "Jahr: " & dtOld.Rows(i).Item("qryGeschäftsjahr").ToString & vbCrLf & "Zeile: " & i + 1 & " - Nr. " & dtOld.Rows(i).Item("lngSort").ToString & vbCrLf & vbCrLf & "EKant Erstellung wird abgebrochen.", vbCritical, "Fehler")
                            Exit Function
                        End If
                    Else
                        'MsgBox("Falscher IVP oder falsche Kostenstelle!" & vbCrLf & vbCrLf & "Arbeitspaket: " & dtOld.Rows(i).Item("txtArbeitspaket").ToString & vbCrLf & "Jahr: " & dtOld.Rows(i).Item("qryGeschäftsjahr").ToString & vbCrLf & "Zeile: " & i + 1 & " - Nr. " & dtOld.Rows(i).Item("lngSort").ToString & vbCrLf & vbCrLf & "EKant Erstellung wird abgebrochen.", vbCritical, "Fehler")
                        MsgBox("Falscher IVP oder falsche Kostenstelle/Geschäftsjahr!" & vbCrLf & vbCrLf & "Arbeitspaket: " & dtOld.Rows(i).Item("txtArbeitspaket").ToString & vbCrLf & "Jahr: " & dtOld.Rows(i).Item("qryGeschäftsjahr").ToString & vbCrLf & "Zeile: " & i + 1 & " - Nr. " & dtOld.Rows(i).Item("lngSort").ToString & vbCrLf & vbCrLf & "EKant Erstellung wird abgebrochen.", vbCritical, "Fehler")    '!ab mod.
                        Exit Function
                    End If
                End If
            Next
#End Region

            Do Until dtOld.Rows.Count = 0 'Bis alle Zeilen in das Ekant Format ueberfuehrt wurden

#Region "Datensatz auf Kostenstelle pruefen"
                Dim GotKostenstelle As Boolean = False
                Dim Arbeitspaket As String = dtOld.Rows(0).Item("qryArbeitspaket")
                Dim Kostenart As String = dtOld.Rows(0).Item("qryKostenart")
                Dim Leistungsart As String = dtOld.Rows(0).Item("txtKst_Lst")
                Dim Kostenstelle As String = dtOld.Rows(0).Item("txtBedarfsnummer")
                Dim Jahr As Integer = dtOld.Rows(0).Item("qryGeschäftsjahr")
                Dim IVP As Double = dtOld.Rows(0).Item("curKosten_pro_Einheit")

                If dtOld.Rows(0).Item("qryKostenart") = "Arbeitskosten" OrElse dtOld.Rows(0).Item("qryKostenart") = "Leistungsarten" Then
                    If Not String.IsNullOrEmpty(dtOld.Rows(0).Item("qryKostenart")) Then
                        GotKostenstelle = True
                    Else
                        GotKostenstelle = False
                        Kostenstelle = ""
                    End If
                Else
                    GotKostenstelle = False
                    Kostenstelle = ""
                End If
#End Region

#Region "Alle Datensaetze zum Suchkriterium finden"
                Dim found() As Data.DataRow
                dtOld.CaseSensitive = False 'Groß und Kleinschreibung ignorieren
                If GotKostenstelle = False Then
                    found = dtOld.Select("qryArbeitspaket = '" & Arbeitspaket & "' and qryKostenart = '" & Kostenart & "' and qryGeschäftsjahr = " & Jahr & " and txtKst_Lst = '" & Leistungsart & "'")
                Else
                    found = dtOld.Select("qryArbeitspaket = '" & Arbeitspaket & "' and qryKostenart = '" & Kostenart & "' and qryGeschäftsjahr = " & Jahr & " and txtKst_Lst = '" & Leistungsart & "' and txtBedarfsnummer = '" & Kostenstelle & "' and curKosten_pro_Einheit ='" & IVP & "'")
                End If
#End Region

#Region "Gefundene Datensaetze in neue Tabelle kopieren und anschliessend aus Ursprungstabelle loeschen"
                If Not IsNothing(found) AndAlso found.Count > 0 Then 'Es gibt mindestens einen Treffer
                    dtEkant.Rows.Add()
                    Dim cRow As Integer = dtEkant.Rows.Count - 1

#Region "Neue Zeile initialisieren und Fixwerte setzen"
                    For j = 0 To dtEkant.Columns.Count - 1
                        dtEkant.Rows(cRow).Item(j) = found(0).Item(j)
                    Next

                    dtEkant.Rows(cRow).Item("txtBeschreibung") = "Summe"
                    dtEkant.Rows(cRow).Item("lngSort") = cRow + 1
                    dtEkant.Rows(cRow).Item("qryPaket") = "none"
                    dtEkant.Rows(cRow).Item("qryEntwicklungspaket") = "none"
                    dtEkant.Rows(cRow).Item("qryKlassifizierung") = "Muss"
                    dtEkant.Rows(cRow).Item("txtBemerkung") = ""
                    dtEkant.Rows(cRow).Item("txtZusatzfeld") = ""
                    dtEkant.Rows(cRow).Item("ID_DATA") = 0
                    dtEkant.Rows(cRow).Item("datErstellungsdatum") = Now
                    If GotKostenstelle = False Then
                        dtEkant.Rows(cRow).Item("txtBedarfsnummer") = ""
                    Else 'Kennzeichen der Kostenstelle in Datensatz schreiben
                        Dim dtIvp As DataRow()
                        'dtIvp = dt.Select("Kostenstelle = '" & found(0).Item("txtBedarfsnummer").ToString & "'")
                        dtIvp = dt.Select("Kostenstelle = '" & found(0).Item("txtBedarfsnummer").ToString & "' And intGJ = " & found(0).Item("qryGeschäftsjahr")) '!ab + GJ
                        'If Not IsNothing(dtIvp) Then
                        If Not IsNothing(dtIvp) AndAlso dtIvp.Count > 0 Then        '!ab mod.
                            dtEkant.Rows(cRow).Item("txtBeschreibung") = dtIvp(0).Item("Abteilung")
                            If Not IsNothing(dtIvp(0).Item("Kst_Schlüssel")) Then
                                If Not String.IsNullOrEmpty(dtIvp(0).Item("Kst_Schlüssel").ToString) Then
                                    dtEkant.Rows(cRow).Item("txtBeschreibung") = dtEkant.Rows(cRow).Item("txtBeschreibung") & " (" & dtIvp(0).Item("Kst_Schlüssel") & ")"
                                End If
                            End If
                        End If
                    End If
#End Region

#Region "Monatswerte berechnen"
                    For i = found.Count - 1 To 0 Step -1 'Datensaetze loeschen, da sie uebertragen wurden
                        For j = 1 To 12
                            Dim Month As tableMonat = ConvertIntegerToDoubleMonth(j)
                            If i = found.Count - 1 Then 'Kopierte Zahlenwerte erstmal nullen
                                dtEkant.Rows(cRow).Item(Month.dblMonat) = 0
                            End If

                            If Not String.IsNullOrEmpty(found(i).Item(Month.dblMonat)) AndAlso Not String.IsNullOrEmpty(found(i).Item("curKosten_pro_Einheit")) Then
                                If GotKostenstelle = True Then
                                    dtEkant.Rows(cRow).Item(Month.dblMonat) = dtEkant.Rows(cRow).Item(Month.dblMonat) + found(i).Item(Month.dblMonat)
                                Else
                                    dtEkant.Rows(cRow).Item(Month.dblMonat) = dtEkant.Rows(cRow).Item(Month.dblMonat) + (found(i).Item(Month.dblMonat) * found(i).Item("curKosten_pro_Einheit"))
                                End If
                            Else
                                dtEkant.Rows(cRow).Item(Month.dblMonat) = dtEkant.Rows(cRow).Item(Month.dblMonat) + 0
                            End If
                        Next

                        If GotKostenstelle = False Then
                            dtEkant.Rows(cRow).Item("curKosten_pro_Einheit") = 1
                            dtEkant.Rows(cRow).Item("AmountPerYear") = 1
                            dtEkant.Rows(cRow).Item("CostsPerYear") = dtEkant.Rows(cRow).Item("dblJanuar") + dtEkant.Rows(cRow).Item("dblFebruar") + dtEkant.Rows(cRow).Item("dblMärz") + dtEkant.Rows(cRow).Item("dblApril") + dtEkant.Rows(cRow).Item("dblMai") + dtEkant.Rows(cRow).Item("dblJuni") + dtEkant.Rows(cRow).Item("dblJuli") + dtEkant.Rows(cRow).Item("dblAugust") + dtEkant.Rows(cRow).Item("dblSeptember") + dtEkant.Rows(cRow).Item("dblOktober") + dtEkant.Rows(cRow).Item("dblNovember") + dtEkant.Rows(cRow).Item("dblDezember")
                        Else
                            dtEkant.Rows(cRow).Item("AmountPerYear") = dtEkant.Rows(cRow).Item("dblJanuar") + dtEkant.Rows(cRow).Item("dblFebruar") + dtEkant.Rows(cRow).Item("dblMärz") + dtEkant.Rows(cRow).Item("dblApril") + dtEkant.Rows(cRow).Item("dblMai") + dtEkant.Rows(cRow).Item("dblJuni") + dtEkant.Rows(cRow).Item("dblJuli") + dtEkant.Rows(cRow).Item("dblAugust") + dtEkant.Rows(cRow).Item("dblSeptember") + dtEkant.Rows(cRow).Item("dblOktober") + dtEkant.Rows(cRow).Item("dblNovember") + dtEkant.Rows(cRow).Item("dblDezember")
                            dtEkant.Rows(cRow).Item("CostsPerYear") = dtEkant.Rows(cRow).Item("curKosten_pro_Einheit") * (dtEkant.Rows(cRow).Item("dblJanuar") + dtEkant.Rows(cRow).Item("dblFebruar") + dtEkant.Rows(cRow).Item("dblMärz") + dtEkant.Rows(cRow).Item("dblApril") + dtEkant.Rows(cRow).Item("dblMai") + dtEkant.Rows(cRow).Item("dblJuni") + dtEkant.Rows(cRow).Item("dblJuli") + dtEkant.Rows(cRow).Item("dblAugust") + dtEkant.Rows(cRow).Item("dblSeptember") + dtEkant.Rows(cRow).Item("dblOktober") + dtEkant.Rows(cRow).Item("dblNovember") + dtEkant.Rows(cRow).Item("dblDezember"))
                        End If

                        found(i).Delete() 'Datensatz loeschen
                    Next
#End Region
                End If
#End Region

            Loop

            ReadOnlyMode = True
            Form_Edit.Reload_iGrid(dtEkant)

            Form_Edit.ToolStripStatusOperation.Text = "E-Kostenantrag erfolgreich erstellt!"
            Form_Edit.StatusStrip.Refresh()
            Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
            Form_Edit.TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("CreateEkant: " & ex.Message.ToString)
        End Try
    End Function

#End Region

    Function ConvertGridtoDataTable(grid As iGrid) As System.Data.DataTable
        Dim i As Integer, j As Integer
        Dim dt As New System.Data.DataTable

        Try
            'Column setup
            If grid.Cols.Count > 0 Then 'Kopier Array muss das erste Mal definiert werden
                For i = 0 To grid.Cols.Count - 1
                    dt.Columns.Add(grid.Cols(i).Key)
                    dt.Columns(i).DataType = grid.Cols(i).CellStyle.ValueType
                Next
            End If
            'Row Copy
            If grid.Rows.Count > 0 Then
                For i = 0 To grid.Rows.Count - 1

                    If grid.Rows(i).Visible = True Then 'Nur sichtbare Daten kopieren
                        If Not grid.Rows(i).Type = iGRowType.AutoGroupRow AndAlso Not grid.Rows(i).Type = iGRowType.ManualGroupRow Then
                            dt.Rows.Add()
                            Dim curDtRow As Integer = dt.Rows.Count - 1

                            For j = 0 To grid.Cols.Count - 1
                                dt.Rows(curDtRow).Item(grid.Cols(j).Key) = grid.Rows(i).Cells(grid.Cols(j).Key).Value
                            Next
                        End If
                    End If

                Next
            End If

            Return dt
        Catch ex As Exception
            MsgBox("ConvertGridtoDataTable: " & ex.Message.ToString)
            Return dt
        End Try
    End Function

    Public Function FormIsLoaded(ByVal sName As String) As Boolean
        Dim bResult As Boolean = False

        ' alle geöffneten Forms durchlauden
        For Each oForm As Form In Application.OpenForms
            If oForm.Name.ToLower = sName.ToLower Then
                bResult = True : Exit For
            End If
        Next

        Return (bResult)
    End Function

End Module
