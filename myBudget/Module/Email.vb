﻿Imports System.Net.Mail
Imports Microsoft.Office.Interop.Outlook

Module Email
    Dim outMail As MailItem

    Public Function SendMails(addressList As String(), subject As String, body As String) As Boolean
        Console.WriteLine("FUNC: Function SendMails")

        If outApp Is Nothing Then Return False 'Outlook Initialisierung hat nicht geklappt

        Dim outMail As MailItem
        Try
            outMail = outApp.CreateItem(OlItemType.olMailItem)
            outMail.DeleteAfterSubmit = True

            outMail.Subject = subject
            outMail.Body = body
            If addressList Is Nothing Then Return False
            For Each address In addressList
                If Not String.IsNullOrWhiteSpace(address) Then  'Sicherheitsprüfung
                    outMail.Recipients.Add(address)
                End If
            Next

            outMail.Send()
            'boolError = False
            Return True
        Catch ex As System.Exception
            'boolError = True
            MsgBox("SendEmails - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Senden von Emails!")
            Return False
        End Try

    End Function

    Public Function CheckAddressFormat(address As String) As Boolean
        Console.WriteLine("FUNC: Function CheckAddressFrmat")
        Try
            Dim temp As MailAddress = New MailAddress(address)
            Return True
        Catch ex As System.Exception
            Return False
        End Try

    End Function
    Public Function InitializeEmailHandling() As Boolean
        Console.WriteLine("FUNC: Function InitialzeEmailHandling")
        Try
            outApp = New Application()
            'boolError = False
            Return True

        Catch ex As System.Exception
            'boolError = True
            MsgBox("InitializeEmailHandling - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Erzeugen des Outlook Objekts!")
            Return False
        End Try
    End Function

    Public Function TerminateEmailHandling() As Boolean
        Console.WriteLine("FUNC: Function TerminateEmailHandling")
        outApp.Quit()
        On Error Resume Next
    End Function
End Module
