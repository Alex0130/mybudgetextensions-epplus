﻿Module Read_Data
    Function Read_all_Data()
        Console.WriteLine("FUNC: Read_all_Data")

        Try
#Region "User Rollen"
            Get_tblAuthorization(True, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"))
#End Region

#Region "Projekte"
            Dim sql As String, where As String
            where = SqlWhereFieldEquals("ID_PROJEKT", pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)
            sql = " where " & where
            Get_tblProjektViaSql(True, "*", sql, True)
#End Region

            Get_Kostenart(True)
            Get_Entwurfstyp(True)
            Get_Klassifizierung(True)
            Get_Team(True)
            Get_Sachkonto(True)
        Catch
            boolError = True
            Exit Function 'Keine Daten
        End Try

    End Function

End Module
