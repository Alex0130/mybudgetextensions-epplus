﻿Imports System.Text
Imports System.Drawing.Text
Imports TenTec.Windows.iGridLib

Public Class iGMultiSelectDropDownEditor
	Implements IiGDropDownControl

    ' Public property to define the choice list
    Public ChoiceList As New List(Of String)

#Region "Private constants and fields"

    Private Const ITEM_SEPARATOR As String = ", "

	' Internal checklist implementation based on iGrid
	Private WithEvents fGridList As New iGrid
	Private Const GRID_LIST_COL_CHECK As Integer = 0
	Private Const GRID_LIST_COL_TITLE As Integer = 1

	Private fParentGrid As iGrid

#End Region

#Region "Control constructor"

	Public Sub New()
		With fGridList
			.BeginUpdate()

			.BorderStyle = iGBorderStyle.None
			.Header.Visible = False
			.GridLines.Mode = iGGridLinesMode.None
			.RowMode = True
			.FocusRect = False
			.ReadOnly = True

			.Cols.Count = 2
			With .Cols(GRID_LIST_COL_CHECK).CellStyle
				.Type = iGCellType.Check
				.ContentIndent = New iGIndent(3, 3, 3, 1)
			End With
			.Cols(GRID_LIST_COL_CHECK).DefaultCellValue = False

			' Fix the width of the checkmark column
			' and auto-resize the item title column:
			.Cols(GRID_LIST_COL_CHECK).Width = 23
			.Cols(GRID_LIST_COL_CHECK).AllowSizing = False
			.AutoResizeCols = True

			.EndUpdate()
		End With
	End Sub

#End Region

#Region "Internal grid list functionality"

	' Toggle the checkmark on mouse clicks
	Private Sub fGridList_CellMouseDown(sender As Object, e As iGCellMouseDownEventArgs) Handles fGridList.CellMouseDown
		ToggleCheckmarkForRow(e.RowIndex)
	End Sub

	' Cancel editing on Escape
	Private Sub fGridList_KeyDown(sender As Object, e As KeyEventArgs) Handles fGridList.KeyDown
		Select Case e.KeyCode
			Case Keys.Enter
				fParentGrid.CommitEditCurCell()
			Case Keys.Escape
				fParentGrid.CancelEditCurCell()
			Case Keys.Space
				If Not IsNothing(fGridList.CurRow) Then
					ToggleCheckmarkForRow(fGridList.CurRow.Index)
				End If
		End Select
	End Sub

	Private Sub ToggleCheckmarkForRow(ByVal rowIndex As Integer)
		Dim myCurValue As Boolean = DirectCast(fGridList.Cells(rowIndex, GRID_LIST_COL_CHECK).Value, Boolean)
		fGridList.Cells(rowIndex, GRID_LIST_COL_CHECK).Value = Not myCurValue
	End Sub

#End Region

#Region "IiGDropDownControl Members"

#Region "Drop-down editor settings"

	''' <summary>
	''' Determines whether to automatically substitute the entered text
	''' with the corresponding value from the drop-down list. 
	''' </summary>
	Public ReadOnly Property AutoSubstitution As Boolean Implements IiGDropDownControl.AutoSubstitution
		Get
			Return False
		End Get
	End Property

	''' <summary>
	''' Determines whether we have the close button in the title bar.
	''' </summary>
	Public ReadOnly Property CloseButton As Boolean Implements IiGDropDownControl.CloseButton
		Get
			Return False
		End Get
	End Property

	''' <summary>
	''' Indicates whether to save the changes in the drop-down control
	''' to the cell after the drop-down form is hidden owing to losing 
	''' of the focus (the user clicked outside of the form or switched
	''' to another application).
	''' </summary>
	Public ReadOnly Property CommitOnHide As Boolean Implements IiGDropDownControl.CommitOnHide
		Get
			Return True
		End Get
	End Property

	''' <summary>
	''' Determines whether to hide the drop-down form if it is opened from a column header.
	''' </summary>
	Public ReadOnly Property HideColHdrDropDown As Boolean Implements IiGDropDownControl.HideColHdrDropDown
		Get
			Return True ' Just in case...
		End Get
	End Property

	''' <summary>
	''' This property should return the image list used by the drop-down control.
	''' The image list will be used by the grid to display the cell image if the 
	''' cell or column style does not contain an image list.
	''' </summary>
	Public ReadOnly Property ImageList As ImageList Implements IiGDropDownControl.ImageList
		Get
			Return Nothing ' We do not use images in this drop-down control
		End Get
	End Property

	''' <summary>
	''' Gets a value indicating whether the drop-down form has resizeable border.
	''' </summary>
	Public ReadOnly Property Sizeable As Boolean Implements IiGDropDownControl.Sizeable
		Get
			Return False
		End Get
	End Property

	''' <summary>
	''' Determines the text in the title bar.
	''' </summary>
	Public ReadOnly Property Text As String Implements IiGDropDownControl.Text
		Get
			Return Nothing
		End Get
	End Property

	''' <summary>
	''' Returns the height of the hosted drop-down control.
	''' </summary>
	Public ReadOnly Property Height As Integer Implements IiGDropDownControl.Height
		Get
			Return fGridList.Height
		End Get
	End Property

	''' <summary>
	''' Returns the width of the hosted drop-down control
	''' (-1 means the width of the current column).
	''' </summary>
	Public ReadOnly Property Width As Integer Implements IiGDropDownControl.Width
		Get
			Return -1 ' Use the column width
		End Get
	End Property

#End Region

#Region "Drop-down control"

    ''' <summary>
    ''' Returns the control to be shown in the drop-down form.
    ''' </summary>
    Public Function GetDropDownControl(grid As iGrid, font As System.Drawing.Font, interfaceType As Type) As Control Implements IiGDropDownControl.GetDropDownControl
        ' We need a reference to the parent grid to commit/cancel editing from the keyboard
        fParentGrid = grid

        ' Return our drop-down control
        Return fGridList
    End Function

#End Region

#Region "Drop-down form events"

    ''' <summary>
    ''' Is raised after the drop-down has been hidden.
    ''' </summary>
    Public Sub OnHide() Implements IiGDropDownControl.OnHide
		' Do almost nothing for this event
		fParentGrid = Nothing ' to avoid resource leaks
	End Sub

	''' <summary>
	''' Is raised after the drop-down has been shown.
	''' </summary>
	Public Sub OnShow() Implements IiGDropDownControl.OnShow
		' Do nothing for this event
	End Sub

	''' <summary>
	''' Is raised when the drop-down is about to be shown.
	''' </summary>
	Public Sub OnShowing() Implements IiGDropDownControl.OnShowing
		' Do nothing for this event
	End Sub

#End Region

#Region "Processing cell values"

	''' <summary>
	''' Gets or sets the selected value.
	''' This property is used before and after editing a cell.
	''' </summary>
	Public Property SelectedItem As Object Implements IiGDropDownControl.SelectedItem
		' The Set accessor is called when the drop-down form is about to be shown.
		' We process the cell value and ChoiceList to prepare our grid list.
		Set(value As Object)
			Dim sCellValue As String = DirectCast(value, String)

			' Get the list of items to check in the ChoiceList
			Dim items As List(Of String)
			If Not String.IsNullOrWhiteSpace(sCellValue) Then
				Dim arrSeparator As String() = New String() {ITEM_SEPARATOR}
				items = sCellValue.Split(arrSeparator, StringSplitOptions.RemoveEmptyEntries).ToList()
			Else
				items = New List(Of String)
			End If

			' Prepare the list of choice items and check
			' them accordingly to the current cell value
			With fGridList
				.BeginUpdate()

				.Rows.Clear()

				For Each s As String In ChoiceList
					Dim myRow As iGRow = .Rows.Add()
					myRow.Cells(GRID_LIST_COL_TITLE).Value = s
					If items.Contains(s) Then
						myRow.Cells(GRID_LIST_COL_CHECK).Value = True
					End If
				Next

				.EndUpdate()
			End With
		End Set

		' The Get accessor is called by iGrid when the user commits editing.
		' We should return the new cell value.
		Get
			Dim builder As New StringBuilder()

			For Each myRow As iGRow In fGridList.Rows
				If DirectCast(myRow.Cells(GRID_LIST_COL_CHECK).Value, Boolean) Then
					If builder.Length > 0 Then
						builder.Append(ITEM_SEPARATOR)
					End If
					builder.Append(DirectCast(myRow.Cells(GRID_LIST_COL_TITLE).Value, String))
				End If
			Next

			Return builder.ToString()
		End Get

	End Property

	''' <summary>
	''' Gets the cell value of the specified drop-down control item.
	''' </summary>
	Public Function GetItemValue(item As Object) As Object Implements IiGDropDownControl.GetItemValue
		Return item ' the drop-down list "item" and corresponding cell value are the same in our case
	End Function

	''' <summary>
	''' Returns a drop-down control item (the cell's AuxValue) that corresponds to the specified cell value.
	''' </summary>
	Public Function GetItemByValue(value As Object, firstByOrder As Boolean) As Object Implements IiGDropDownControl.GetItemByValue
		Return value ' the drop-down list "item" and corresponding cell value are the same in our case
	End Function

	''' <summary>
	''' Returns a drop-down control item that corresponds to the specified text.
	''' This method is invoked when the user enters a text into a cell.
	''' </summary>
	Public Function GetItemByText(text As String) As Object Implements IiGDropDownControl.GetItemByText
		Return Nothing ' The GetItemByText member is not actually used in our editor
	End Function

	''' <summary>
	''' Gets the image index of the specified drop-down control item.
	''' </summary>
	Public Function GetItemImageIndex(item As Object) As Integer Implements IiGDropDownControl.GetItemImageIndex
		Return -1 ' no images in this editor
	End Function

#End Region

#End Region

    'Public Sub SetTextRenderingHint(textRenderingHint As Drawing.Text.TextRenderingHint) Implements IiGDropDownControl.SetTextRenderingHint
    '    fGridList.TextRenderingHint = textRenderingHint
    'End Sub

    Public Sub SetTextRenderingHint(textRenderingHint As TextRenderingHint) Implements IiGDropDownControl.SetTextRenderingHint
        fGridList.TextRenderingHint = textRenderingHint
        'Throw New NotImplementedException()
    End Sub
End Class
