﻿Imports myBudget.UI

Public Class ArbeitspaketForm

#Region "Events"
    Private Sub ArbeitspaketForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub ArbeitspaketForm_Load")

        '1. Daten anzeigen und laden:
        '- Projekte anzeigen
        '- Arbeitspakete und PSP-Elemente abhängig vom Projekt laden & anzeigen
        '- AP Felder abhängig vom gewählten AP anzeigen
        '- PSP Elemten anzeigen

        Try
            '1.
            Try
                If intArt_der_Editierung = 0 Or intArt_der_Editierung = 1 Then 'Neu und Änderung
                    ViewData_Form_Projekt(0)
                    ViewData_Form_Arbeitspaket(0)
                Else 'VIEW: Daten zum AP vorhanden
                    Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & pbl_IdProjekt & "")
                    If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                        Dim IndexProjekt As Integer = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                        ViewData_Form_Projekt(IndexProjekt)
                    End If

                    Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
                    Dim RowIndex As Integer = -1
                    If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                        RowIndex = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
                        ViewData_Form_Arbeitspaket(RowIndex)
                    End If
                End If
            Catch ex As Exception
                MsgBox("ArbeitspaketForm_Load - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Sub 'Keine Daten zum laden
            End Try


            'Abhängig von der Art der Bearbeitung werden Felder befüllt oder nicht
            If intArt_der_Editierung = 0 Then 'Neuer Eintrag -> keine Daten zum AP vorhanden
                Form_Arbeitspaket.Text = "Erstellung Arbeitspaket"
                ComboProjekt.Enabled = True
                ComboArbeitspaket.Enabled = False
                ComboArbeitspaket.Visible = False
                lblArbeitspaket.Visible = False
                txtPSP.Enabled = True
                ComboTeam.Enabled = True
                txtArbeitspaket.Enabled = True
                txtPrämissen.Enabled = True
                txtVerantwortlich.Enabled = True
                cmdSaveData.Visible = True
                cmdSaveData.Text = "Erstellen"
                cbxInactive.Visible = False
            ElseIf intArt_der_Editierung = 1 Then 'Bearbeitung
                Form_Arbeitspaket.Text = "Änderung Arbeitspaket"
                ComboProjekt.Enabled = True
                ComboArbeitspaket.Enabled = True
                ComboArbeitspaket.Visible = True
                lblArbeitspaket.Visible = True
                txtPSP.Enabled = True
                ComboTeam.Enabled = True
                txtArbeitspaket.Enabled = True
                txtPrämissen.Enabled = True
                txtVerantwortlich.Enabled = True
                cmdSaveData.Visible = True
                cmdSaveData.Text = "Ändern"
                cbxInactive.Visible = True
            ElseIf intArt_der_Editierung = 2 Then 'View
                Form_Arbeitspaket.Text = "Anzeige Arbeitspaket"
                ComboProjekt.Enabled = False
                ComboArbeitspaket.Enabled = False
                ComboArbeitspaket.Visible = False
                lblArbeitspaket.Visible = False
                txtPSP.Enabled = False
                ComboTeam.Enabled = False
                txtArbeitspaket.Enabled = False
                txtPrämissen.Enabled = False
                txtVerantwortlich.Enabled = False
                cmdSaveData.Visible = False
                cbxInactive.Visible = True
                cbxInactive.Enabled = False
            ElseIf intArt_der_Editierung = 3 Then 'Delete
                Form_Arbeitspaket.Text = "Arbeitspaket Löschen"
                ComboProjekt.Enabled = True
                ComboArbeitspaket.Enabled = True
                ComboArbeitspaket.Visible = True
                lblArbeitspaket.Visible = True
                txtPSP.Enabled = False
                ComboTeam.Enabled = False
                txtArbeitspaket.Enabled = False
                txtPrämissen.Enabled = False
                txtVerantwortlich.Enabled = False
                cmdSaveData.Visible = True
                cmdSaveData.Text = "Löschen"
                cbxInactive.Visible = True
                cbxInactive.Enabled = False
            End If
        Catch ex As Exception
            MsgBox("ArbeitspaketForm_Load - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            Exit Sub 'Keine Daten zum laden
        End Try
    End Sub

    Public Sub ComboProjekt_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboProjekt.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboProjekt_SelectedIndexChanged")

        Try
            ViewData_Form_Projekt(ComboProjekt.SelectedIndex)
            ViewData_Form_Arbeitspaket(0)
        Catch ex As Exception
            MsgBox("ComboProjekt_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub ComboArbeitspaket_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboArbeitspaket.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboArbeitspaket_SelectedIndexChanged")

        Try
            ViewData_Form_Arbeitspaket(ComboArbeitspaket.SelectedIndex)
        Catch ex As Exception
            MsgBox("ComboArbeitspaket_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Aktionen"
    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Console.WriteLine("CLICK: Private Sub cmdSaveData_Click")

        Dim i As Integer
        Dim strTeam As String
        Dim Latest_ID_AP As Long

        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            '1. Daten prüfen
            '2. Daten speichern
            boolError = False

            If intArt_der_Editierung = 0 Then
                If txtArbeitspaket.Text = "" Then
                    MsgBox("Bitte einen Namen für das Arbeitspaket eingeben", MsgBoxStyle.Critical, "Fehler beim Speichern")
                    Exit Sub
                End If
                If ComboTeam.SelectedIndex < 0 Then
                    MsgBox("Es wurde kein Team für das Arbeitspaket gewählt", MsgBoxStyle.Critical, "Fehler beim Speichern")
                    Exit Sub
                End If
            End If

#Region "Neues Arbeitspaket anelgen"
            If intArt_der_Editierung = 0 Then 'NEUES AP ANLEGEN
                Dim NewID_AP As Integer = -1
                Try
                    NewID_AP = Add_Arbeitspaket(txtArbeitspaket.Text, txtPrämissen.Text, txtVerantwortlich.Text, Now, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), txtPSP.Text, pbl_Team(ComboTeam.SelectedIndex).txtTeam)
                    If Not InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 AndAlso NewID_AP >= 0 Then 'Berechtigung für das angelegte Arbeitspaket erzeugen
                        Add_Authorization("ID_AP", NewID_AP, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), "Write")
                        pbl_dtAuth = Get_tblAuthorization(True, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"))
                    End If

                    If boolError = False Then 'Wenn Daten erfolgreich gespeichert wurden
                        Try
                            Get_tblArbeitspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False) 'MIT ID_PROJEKT laden
                            Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
                            Dim RowIndex As Integer = -1
                            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                                RowIndex = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
                                pbl_IdAp = pbl_dtArbeitspaket.Rows(RowIndex).Item("ID_AP")
                                ViewCombo.View_ComboboxArbeitspaket(Form_Edit.ComboArbeitspaket, RowIndex, pbl_dtArbeitspaket)
                                ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, RowIndex, pbl_dtArbeitspaket)
                            End If

                        Catch ex As Exception
                            MsgBox("NeuToolStripMenuItem8_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                        End Try

                        Form_Edit.ToolStripStatusOperation.Text = "Arbeitspaket erfolgreich hinzugefügt"
                        Form_Edit.StatusStrip.Refresh()
                        MsgBox("Arbeitspaket erfolgreich angelegt!", vbInformation, "Erfolg")
                    End If

                Catch ex As Exception
                    boolError = True
                    MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                    Exit Sub 'Kein Arbeitspaket hinzugefügt
                End Try

                If NewID_AP >= 0 Then
                    Try 'Abhängigkeiten von Arbeitspaket: Paket und AP Versionen
                        Add_Paket("none", "Initiales Paket", NewID_AP)
                        For i = 0 To UBound(pbl_Entwurf)
                            Add_VersionArbeitspaket(NewID_AP, 1, "Initiale Version", pbl_Entwurf(i).txtEntwurfstyp)
                        Next
                    Catch ex As Exception
                        boolError = True
                        MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                    End Try
                End If
#End Region

#Region "Bestehendes Arbeitspaket ändern"
            ElseIf intArt_der_Editierung = 1 Then 'BESTEHENDES AP ÄNDERN
                Try
                    If ComboTeam.SelectedIndex < 0 Then
                        strTeam = ""
                    Else
                        strTeam = pbl_Team(ComboTeam.SelectedIndex).txtTeam
                    End If
                    Update_Arbeitspaket(pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), txtArbeitspaket.Text, txtPrämissen.Text, txtVerantwortlich.Text, txtPSP.Text, strTeam, cbxInactive.Checked)

                    If boolError = False Then
                        Get_tblArbeitspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False)
                        Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
                        Dim RowIndex As Integer = -1
                        If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                            pbl_IdAp = pbl_dtArbeitspaket.Rows(pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))).Item("ID_AP")
                        End If

                        Form_Edit.ToolStripStatusOperation.Text = "Arbeitspaket erfolgreich geändert"
                        Form_Edit.StatusStrip.Refresh()
                        MsgBox("Arbeitspaket erfolgreich geändert!", vbInformation, "Erfolgreich")
                    End If


                Catch ex As Exception
                    boolError = True
                    MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
#End Region

#Region "Bestehendes Arbeitspaket löschen"
            ElseIf intArt_der_Editierung = 3 Then
                Try
                    '1. Prüfen ob Datensätze vorhanden sind
                    '2. Versionen löschen
                    '3. Pakete löschen
                    '4. Authorisierungen löschen

                    Dim ID_AP As Long = pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP")

                    '1
                    Dim dt As New System.Data.DataTable
                    dt = Get_tblDataViaSql("*", " where qryArbeitspaket = " & ID_AP & "")
                    If dt.Rows.Count > 0 Then
                        MsgBox("Es existieren bereits Daten zu diesem Arbeitspaket. Bitte wenden Sie sich an Ihren Administrator.", vbCritical, "Fehler")
                        Exit Sub
                    End If

                    '2 + 3 + 4 Löschen
                    If Delete_VersionArbeitspaket(ID_AP) = False Then
                        MsgBox("Fehler beim Löschen der Versionen. Bitte wenden Sie sich an Ihren Administrator.", vbCritical, "Fehler")
                        Exit Sub
                    End If
                    If Delete_Paket(ID_AP) = False Then
                        MsgBox("Fehler beim Löschen der Pakete. Bitte wenden Sie sich an Ihren Administrator.", vbCritical, "Fehler")
                        Exit Sub
                    End If
                    If Delete_ArbeitspaketAuth(ID_AP) = False Then
                        MsgBox("Fehler beim Löschen der Berechtigungen. Bitte wenden Sie sich an Ihren Administrator.", vbCritical, "Fehler")
                        Exit Sub
                    End If
                    If Delete_Arbeitspaket(ID_AP) = False Then
                        MsgBox("Fehler beim Löschen des Arbeitspakets. Bitte wenden Sie sich an Ihren Administrator.", vbCritical, "Fehler")
                        Exit Sub
                    End If

                    If boolError = False Then
                        Get_tblArbeitspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False)
                        pbl_IdAp = pbl_dtArbeitspaket.Rows(ViewCombo.View_ComboboxArbeitspaket(Form_Edit.ComboArbeitspaket, 0, pbl_dtArbeitspaket)).Item("ID_AP")
                        ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, 0, pbl_dtArbeitspaket)

                        Form_Edit.ToolStripStatusOperation.Text = "Arbeitspaket erfolgreich gelöscht"
                        Form_Edit.StatusStrip.Refresh()
                        MsgBox("Arbeitspaket erfolgreich gelöscht!", vbInformation, "Erfolgreich")
                    End If

                    'Lösche Notifications des Paketes
                    RefreshSubscriptionState(EditForm.EmailNotificationState)   '!ab added
                Catch ex As Exception
                    boolError = True
                    MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
#End Region
            End If

        Catch ex As Exception
            MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try

    End Sub

#End Region

#Region "Methoden"
    Function ViewData_Form_Arbeitspaket(Index_Arbeitspaket As Integer)
        Console.WriteLine("FUNC: Function ViewData_Form_Arbeitspaket")

        'Es müssen keine Daten neu geladen werden
        'Abhängige Daten anzeigen
        Dim Index_Team As Integer

        Try
            Try
                Index_Arbeitspaket = ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, Index_Arbeitspaket, pbl_dtArbeitspaket) 'AP combo laden
            Catch ex As Exception
                MsgBox("ViewData_Form_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Function
            End Try

            txtArbeitspaket.Text = ""
            txtPrämissen.Text = ""
            txtVerantwortlich.Text = ""
            ComboTeam.Items.Clear()
            ComboTeam.Text = ""
            txtPSP.Text = ""

            If intArt_der_Editierung = 0 Then
                Index_Arbeitspaket = -1 'Zum gewählten Arbeitspaket keine Daten laden
                ViewCombo.View_ComboboxTeam(ComboTeam, 0, pbl_Team)
            End If

            If Index_Arbeitspaket < 0 Then Exit Function 'Es gibt (noch) keine Daten zum Arbeitspaket

            txtArbeitspaket.Text = pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("txtArbeitspaket")
            txtPrämissen.Text = pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("txtPrämissen")
            txtVerantwortlich.Text = pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("txtVerantwortlich")
            txtPSP.Text = pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("txtPSP_Element")
            If IsDBNull(pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("boolInactive")) Then
                cbxInactive.Checked = False
            ElseIf pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("boolInactive") = True Then
                cbxInactive.Checked = True
            Else
                cbxInactive.Checked = False
            End If

            Try
                'Zugehöriges Team
                Index_Team = Array.FindIndex(pbl_Team, Function(f) f.txtTeam = pbl_dtArbeitspaket.Rows(Index_Arbeitspaket).Item("qryTeam"))
                Index_Team = ViewCombo.View_ComboboxTeam(ComboTeam, Index_Team, pbl_Team)
            Catch ex As Exception
                Index_Team = -1
                MsgBox("ViewData_Form_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            End Try
        Catch ex As Exception
            MsgBox("ViewData_Form_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            Exit Function
        End Try
    End Function

    Function ViewData_Form_Projekt(Index_Projekt As Integer)
        Console.WriteLine("FUNC: Function ViewData_Form_Projekt")
        'Es müssen zugehörige Arbeitspakete und PSP Elemente neu geladen werden
        'Abhängige Daten anzeigen

        Try
            If Index_Projekt < 0 Then Exit Function 'Kein Projekt --> keine View oder Eingabe möglich

            'Projekte müssen nicht geladen werden, da diese bei der Useranmeldung gezogen werden, basierend auf Berechtigungen
            Try
                Index_Projekt = ViewCombo.View_ComboboxProjekt(ComboProjekt, Index_Projekt, pbl_dtProjekt) 'Projekte anzeigen
            Catch ex As Exception
                MsgBox("ViewData_Form_Projekt - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Function 'Keine Projekte verfügbar
            End Try

            'Get Daten abhängig vom Projekte
            Get_tblArbeitspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, True) 'AP zum Projekt laden

            'Daten abhängig vom Projekt initialisieren die erstmal unabhängig vom AP sind
            ViewCombo.View_ComboboxTeam(ComboTeam, 0, pbl_Team)

        Catch ex As Exception
            MsgBox("ViewData_Form_Projekt - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Function
#End Region

End Class
