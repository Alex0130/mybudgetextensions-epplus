﻿Imports System.Drawing

Public Class DatenVergleichForm
#Region "Events"
    Private Sub DatenVergleichForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub DatenVergleichForm_Load")

        Dim i As Long
        Dim IndexEntwurf As Integer = 0
        Dim pbl_ListVersionAp As tableVersionArbeitspaket()
        pbl_ListVersionAp = Get_VersionArbeitspaket(False, pbl_IdAp, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp)

        Try
            ComboVersion.Items.Clear()
            ComboEntwurf.Items.Clear()
            For i = LBound(pbl_ListVersionAp) To UBound(pbl_ListVersionAp)
                ComboVersion.Items.Add(pbl_ListVersionAp(i).lngVersion)
            Next
            ComboVersion.SelectedIndex = 0
            For i = LBound(pbl_Entwurf) To UBound(pbl_Entwurf)
                If Not pbl_Entwurf(i).DatenHerkunft = "Ist" Then
                    ComboEntwurf.Items.Add(pbl_Entwurf(i).ReportBeschreibung)
                    If pbl_Entwurf(i).txtEntwurfstyp = pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp Then
                        IndexEntwurf = i
                    End If
                End If
            Next
            RemoveHandler ComboEntwurf.SelectedIndexChanged, AddressOf ComboEntwurf_SelectedIndexChanged
            ComboEntwurf.SelectedIndex = IndexEntwurf
            AddHandler ComboEntwurf.SelectedIndexChanged, AddressOf ComboEntwurf_SelectedIndexChanged
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Aktionen"
    Private Sub SaveData_Click(sender As Object, e As EventArgs) Handles SaveData.Click
        Console.WriteLine("AKTION: Private Sub SaveData_Click")

        '1. Alle Filter löschen
        '2. Neue Daten in extra table herunterladen
        '3. Daten vergleichen

        Dim i As Integer, j As Integer
        Dim grid As TenTec.Windows.iGridLib.iGrid
        grid = Form_Edit.iGrid

#Region "Prüfungen"
        'Prüfen ob eine aktive Verbindung zum Server existiert
        If con.State = ConnectionState.Closed Or boolConnected = False Then
            MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            Exit Sub
        End If

        If boolCompareMode = True Then
            MsgBox("Datenvergleich nur im Editor Modus möglich." & vbCrLf & "Bitte den Vergleich Modus beenden.", Title:="Fehler beim Vergleich", Buttons:=MsgBoxStyle.Critical)
            Exit Sub
        End If
#End Region

        '1.
        RemoveHandler Form_Edit.iGrid.BeforeContentsGrouped, AddressOf Form_Edit.fGrid_BeforeContentsGrouped
        Form_Edit.iGrid_ClearFilterAndGroups() 'Alle Daten anzeigen
        RemoveHandler Form_Edit.iGrid.BeforeContentsGrouped, AddressOf Form_Edit.fGrid_BeforeContentsGrouped

        '2.
        Dim dt_Vgl As System.Data.DataTable 'Vergleichsdaten
        dt_Vgl = Get_tblData(False, pbl_IdAp, ComboVersion.Text, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)

#Region "Abbruchkriterien"
        If IsNothing(dt_Vgl) Then Exit Sub

        If grid.Rows.Count = 0 Or dt_Vgl.Rows.Count = 0 Then
            Exit Sub
        End If
        If grid.Cols.Count = 0 Or dt_Vgl.Columns.Count = 0 Then
            Exit Sub
        End If
#End Region

        Dim RefRow As Integer
        Dim ColName As String

#Region "Vergleichstabelle als Grundlage"
        Form_Edit.iGrid_RemoveSumRows()

        For i = 0 To dt_Vgl.Rows.Count - 1 'Vergleichstabelle als Grundlage
            RefRow = -1
            For j = 0 To grid.Rows.Count - 1 'Vergleichswert suchen
                If dt_Vgl(i)("txtBeschreibung") = grid.Rows(j).Cells("txtBeschreibung").Value Then
                    If dt_Vgl(i)("qryKostenart") = grid.Rows(j).Cells("qryKostenart").Value Then
                        If dt_Vgl(i)("qryGeschäftsjahr") = grid.Rows(j).Cells("qryGeschäftsjahr").Value Then
                            RefRow = j
                            Exit For 'j
                        End If
                    End If
                End If
            Next

            If RefRow >= 0 Then 'Vergleich möglich
                For j = 0 To dt_Vgl.Columns.Count - 1 'Alte Daten als Basis für den Vergleich
                    ColName = dt_Vgl.Columns(j).ColumnName.ToString
                    If Not dt_Vgl.Rows(i)(j) = grid.Rows(i).Cells(ColName).Value Then 'Abweichung gefunden
                        If dt_Vgl.Columns(j).DataType = GetType(Double) Then 'Berechnung möglich
                            If dt_Vgl.Rows(i)(j) > grid.Rows(i).Cells(ColName).Value Then
                                grid.Rows(i).Cells(ColName).BackColor = Color.LightBlue
                            Else
                                grid.Rows(i).Cells(ColName).BackColor = Color.LightPink
                            End If
                        Else 'Nur farblich kennzeichnen
                            grid.Rows(i).Cells(ColName).BackColor = Color.LightGoldenrodYellow
                        End If
                    End If
                Next
            Else 'Zeile existiert in der neuen Version nicht
                Form_Edit.iGrid_NewRow(True, 0) 'Neue Zeile einfügen
                Dim newRow As Integer = grid.Rows.Count - 1

                For j = 0 To dt_Vgl.Columns.Count - 1
                    ColName = dt_Vgl.Columns(j).ColumnName.ToString
                    grid.Rows(newRow).Cells(ColName).Value = dt_Vgl(i)(j)
                Next
                grid.Rows(newRow).CellStyle.BackColor = Color.LightBlue
            End If
        Next
#End Region

#Region "Grid als Grundlage"
        For i = 0 To grid.Rows.Count - 1 'Grid als Grundlage: Prüfen welche Daten nur im Grid, nicht aber in den Vergleichsdaten vorhanden sind
            RefRow = -1
            For j = 0 To dt_Vgl.Rows.Count - 1 'Vergleichswert suchen
                If dt_Vgl(j)("txtBeschreibung") = grid.Rows(i).Cells("txtBeschreibung").Value Then
                    If dt_Vgl(j)("qryKostenart") = grid.Rows(i).Cells("qryKostenart").Value Then
                        If dt_Vgl(j)("qryGeschäftsjahr") = grid.Rows(i).Cells("qryGeschäftsjahr").Value Then
                            RefRow = j
                            Exit For 'j
                        End If
                    End If
                End If
            Next
            If RefRow = -1 Then 'Zeile ist neu
                grid.Rows(i).CellStyle.BackColor = Color.LightPink
            End If
        Next
#End Region

        ReadOnlyMode = True
        Form_Edit.iGrid_EnableEditFunctions(False, False, "Nur Leserechte")

        Me.Close()
    End Sub
#End Region

#Region "Events"
    Private Sub ComboEntwurf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboEntwurf.SelectedIndexChanged
        Console.WriteLine("FUNC: Private Sub ComboEntwurf_SelectedIndexChanged")

        Dim i As Long
        Dim pbl_ListVersionAp As tableVersionArbeitspaket()

        Try
            pbl_ListVersionAp = Get_VersionArbeitspaket(False, pbl_IdAp, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)
            ComboVersion.Items.Clear()
            For i = LBound(pbl_ListVersionAp) To UBound(pbl_ListVersionAp)
                ComboVersion.Items.Add(pbl_ListVersionAp(i).lngVersion)
            Next
            ComboVersion.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub
#End Region
End Class