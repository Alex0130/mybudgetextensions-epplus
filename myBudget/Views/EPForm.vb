﻿Public Class EPForm
#Region "Events"
    Private Sub EPForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub EPForm_Load")

        '1.
        Try
            If intArt_der_Editierung = 0 Or intArt_der_Editierung = 1 Then 'Neu und Änderung
                ViewData_Form_Projekt(0)
                ViewData_Form_Entwicklungspaket(0)
            Else 'VIEW: Daten zum AP vorhanden

                Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & pbl_IdProjekt & "")
                If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                    Dim IndexProjekt As Integer = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                    ViewData_Form_Projekt(IndexProjekt)
                End If

                ViewData_Form_Entwicklungspaket(Array.FindIndex(pbl_Ep, Function(f) f.txtEntwicklungspaket = pbl_txtEp))
            End If
        Catch ex As Exception
            MsgBox("EPForm_Load - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            Exit Sub 'Keine Daten zum laden
        End Try


        'Abhängig von der Art der Bearbeitung werden Felder befüllt oder nicht
        If intArt_der_Editierung = 0 Then 'Neuer Eintrag -> keine Daten zum AP vorhanden
            Form_Entwicklungspaket.Text = "Erstellung Entwicklungspaket"
            ComboProjekt.Enabled = True
            ComboEntwicklungspaket.Enabled = False
            ComboEntwicklungspaket.Visible = False
            lblEntwicklungspaket.Visible = False
            txtEntwicklungspaket.Enabled = True
            txtBeschreibung.Enabled = True
            cmdSaveData.Visible = True
        ElseIf intArt_der_Editierung = 1 Then 'Bearbeitung
            Form_Entwicklungspaket.Text = "Änderung Entwicklungspaket"
            ComboProjekt.Enabled = True
            ComboEntwicklungspaket.Enabled = True
            ComboEntwicklungspaket.Visible = True
            lblEntwicklungspaket.Visible = True
            txtEntwicklungspaket.Enabled = True
            txtBeschreibung.Enabled = True
            cmdSaveData.Visible = True
        Else 'View
            Form_Entwicklungspaket.Text = "Anzeige Entwicklungspaket"
            ComboProjekt.Enabled = False
            ComboEntwicklungspaket.Enabled = False
            ComboEntwicklungspaket.Visible = False
            lblEntwicklungspaket.Visible = False
            txtEntwicklungspaket.Enabled = False
            txtBeschreibung.Enabled = False
            cmdSaveData.Visible = False
        End If
    End Sub

    Public Sub ComboProjekt_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboProjekt.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboProjekt_SelectedIndexChanged")

        Try
            ViewData_Form_Projekt(ComboProjekt.SelectedIndex)
            ViewData_Form_Entwicklungspaket(0)
        Catch ex As Exception
            MsgBox("ComboProjekt_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub ComboEntwicklungspaket_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboEntwicklungspaket.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboEntwicklungspaket_SelectedIndexChanged")

        Try
            ViewData_Form_Entwicklungspaket(ComboEntwicklungspaket.SelectedIndex)
        Catch ex As Exception
            MsgBox("ComboEntwicklungspaket_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Aktionen"
    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Console.WriteLine("AKTION: Private Sub cmdSaveData_Click")

        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            '1. Daten prüfen
            '2. Daten speichern
            boolError = False

            If txtEntwicklungspaket.Text = "" Then
                MsgBox("Bitte einen Namen für das Entwicklungsplaket eingeben", MsgBoxStyle.Critical, "Fehler beim Speichern")
                Exit Sub
            End If
            If ComboProjekt.SelectedIndex < 0 Then
                MsgBox("Bitte wählen Sie ein Projekt", MsgBoxStyle.Critical, "Fehler beim Speichern")
                Exit Sub
            End If

            If intArt_der_Editierung = 0 Then
                Try
                    Add_Entwicklungspaket(txtEntwicklungspaket.Text, txtBeschreibung.Text, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"))
                Catch ex As Exception
                    MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
            Else 'Änderung EP
                Try
                    Update_Entwicklungspaket(txtEntwicklungspaket.Text, txtBeschreibung.Text, ComboEntwicklungspaket.Text, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT")) '
                    Update_Data_Entwicklungspaket(pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), txtEntwicklungspaket.Text, ComboEntwicklungspaket.Text)
                Catch ex As Exception
                    MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
            End If
        Catch ex As Exception
            MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try

        Me.Close()
    End Sub
#End Region

#Region "Methoden"
    Function ViewData_Form_Entwicklungspaket(Index_Entwicklungspaket As Integer)
        Console.WriteLine("FUNC: Function ViewData_Form_Entwicklungspaket")

        'Es müssen keine Daten neu geladen werden
        'Abhängige Daten anzeigen
        Try
            Try
                Index_Entwicklungspaket = ViewCombo.View_ComboboxEntwicklungspaket(ComboEntwicklungspaket, Index_Entwicklungspaket, pbl_Ep) 'EP combo laden
            Catch ex As Exception
                MsgBox("ViewData_Form_Entwicklungspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Function
            End Try

            txtEntwicklungspaket.Text = ""
            txtBeschreibung.Text = ""

            If intArt_der_Editierung = 0 Then
                Index_Entwicklungspaket = -1 'Zum gewählten EP keine Daten laden
            End If

            If Index_Entwicklungspaket < 0 Then Exit Function 'Es gibt (noch) keine Daten zum Arbeitspaket

            txtEntwicklungspaket.Text = pbl_Ep(Index_Entwicklungspaket).txtEntwicklungspaket
            txtBeschreibung.Text = pbl_Ep(Index_Entwicklungspaket).txtBeschreibung
        Catch ex As Exception
            MsgBox("ViewData_Form_Entwicklungspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Function

    Function ViewData_Form_Projekt(Index_Projekt As Integer)
        Console.WriteLine("FUNC: Function ViewData_Form_Projekt")

        'Es müssen zugehörige Arbeitspakete und PSP Elemente neu geladen werden
        'Abhängige Daten anzeigen

        If Index_Projekt < 0 Then Exit Function 'Kein Projekt --> keine View oder Eingabe möglich

        'Projekte müssen nicht geladen werden, da diese bei der Useranmeldung gezogen werden, basierend auf Berechtigungen
        Try
            Index_Projekt = ViewCombo.View_ComboboxProjekt(ComboProjekt, Index_Projekt, pbl_dtProjekt) 'Projekte anzeigen
            Get_Entwicklungspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"))
        Catch ex As Exception
            MsgBox(ex.Message.ToString, vbCritical, "Fehler")
            Exit Function 'Keine Projekte verfügbar
        End Try
    End Function
#End Region
End Class