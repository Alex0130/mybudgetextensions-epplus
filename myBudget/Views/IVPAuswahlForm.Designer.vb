﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class IVPAuswahlForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IVPAuswahlForm))
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.comboIvp = New System.Windows.Forms.ComboBox()
        Me.lblArbeitspaket = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.Maroon
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(292, 58)
        Me.cmdSaveData.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 11
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'comboIvp
        '
        Me.comboIvp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboIvp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboIvp.FormattingEnabled = True
        Me.comboIvp.Location = New System.Drawing.Point(13, 29)
        Me.comboIvp.Margin = New System.Windows.Forms.Padding(4)
        Me.comboIvp.Name = "comboIvp"
        Me.comboIvp.Size = New System.Drawing.Size(379, 21)
        Me.comboIvp.TabIndex = 9
        '
        'lblArbeitspaket
        '
        Me.lblArbeitspaket.AutoSize = True
        Me.lblArbeitspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArbeitspaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblArbeitspaket.Location = New System.Drawing.Point(13, 9)
        Me.lblArbeitspaket.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblArbeitspaket.Name = "lblArbeitspaket"
        Me.lblArbeitspaket.Size = New System.Drawing.Size(65, 16)
        Me.lblArbeitspaket.TabIndex = 10
        Me.lblArbeitspaket.Text = "Auswahl"
        '
        'IVPAuswahlForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(404, 106)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Controls.Add(Me.comboIvp)
        Me.Controls.Add(Me.lblArbeitspaket)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "IVPAuswahlForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Eindeutigen IVP auswählen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
    Friend WithEvents comboIvp As ComboBox
    Friend WithEvents lblArbeitspaket As System.Windows.Forms.Label
End Class
