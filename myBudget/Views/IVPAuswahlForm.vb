﻿Public Class IVPAuswahlForm
    Private Sub IVPAuswahlForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If dtIVP.Count = 0 Then Exit Sub

            comboIvp.Items.Clear()

            Dim i As Integer
            For i = 0 To dtIVP.Count - 1
                comboIvp.Items.Add(dtIVP(i).Item("Abteilung") & ", " & dtIVP(i).Item("Kostenstelle") & " - " & dtIVP(i).Item("Bezeichnung") & ": " & dtIVP(i).Item("Std_Satz") & "€/h")
            Next
            comboIvp.SelectedIndex = 0

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Try
            IVP = dtIVP(comboIvp.SelectedIndex).Item("Std_Satz")
        Catch ex As Exception
            IVP = 1
        End Try
        Me.Close()
    End Sub
End Class