﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KopierenForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(KopierenForm))
        Me.ListGJ = New System.Windows.Forms.CheckedListBox()
        Me.ListVersion = New System.Windows.Forms.CheckedListBox()
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboEntwurf = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.comboAp = New System.Windows.Forms.ComboBox()
        Me.lblAp = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ListGJ
        '
        Me.ListGJ.FormattingEnabled = True
        Me.ListGJ.Location = New System.Drawing.Point(149, 178)
        Me.ListGJ.Name = "ListGJ"
        Me.ListGJ.Size = New System.Drawing.Size(120, 124)
        Me.ListGJ.TabIndex = 2
        '
        'ListVersion
        '
        Me.ListVersion.FormattingEnabled = True
        Me.ListVersion.Location = New System.Drawing.Point(11, 178)
        Me.ListVersion.Name = "ListVersion"
        Me.ListVersion.Size = New System.Drawing.Size(120, 124)
        Me.ListVersion.TabIndex = 1
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.DarkRed
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(169, 308)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 25
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(8, 159)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 16)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Version"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(146, 159)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 16)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Geschäftsjahr"
        '
        'ComboEntwurf
        '
        Me.ComboEntwurf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboEntwurf.FormattingEnabled = True
        Me.ComboEntwurf.Location = New System.Drawing.Point(11, 123)
        Me.ComboEntwurf.Name = "ComboEntwurf"
        Me.ComboEntwurf.Size = New System.Drawing.Size(120, 21)
        Me.ComboEntwurf.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkRed
        Me.Label4.Location = New System.Drawing.Point(11, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 16)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Entwurfstyp"
        '
        'comboAp
        '
        Me.comboAp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.comboAp.FormattingEnabled = True
        Me.comboAp.Location = New System.Drawing.Point(11, 80)
        Me.comboAp.Name = "comboAp"
        Me.comboAp.Size = New System.Drawing.Size(258, 21)
        Me.comboAp.TabIndex = 31
        '
        'lblAp
        '
        Me.lblAp.AutoSize = True
        Me.lblAp.BackColor = System.Drawing.Color.White
        Me.lblAp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAp.ForeColor = System.Drawing.Color.DarkRed
        Me.lblAp.Location = New System.Drawing.Point(11, 61)
        Me.lblAp.Name = "lblAp"
        Me.lblAp.Size = New System.Drawing.Size(96, 16)
        Me.lblAp.TabIndex = 32
        Me.lblAp.Text = "Arbeitspaket"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(11, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(258, 52)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Kopiert die aktuell angezeigten Daten nach:"
        '
        'KopierenForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(281, 355)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.comboAp)
        Me.Controls.Add(Me.lblAp)
        Me.Controls.Add(Me.ListGJ)
        Me.Controls.Add(Me.ListVersion)
        Me.Controls.Add(Me.ComboEntwurf)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "KopierenForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Kopieren nach:"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListGJ As System.Windows.Forms.CheckedListBox
    Friend WithEvents ListVersion As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboEntwurf As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents comboAp As ComboBox
    Friend WithEvents lblAp As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
