﻿Public Class KopierenForm
#Region "Events"
    Private Sub KopierenForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub KopierenForm_Load")

        Dim Index_Entwurf As Integer
        Dim Index_Arbeitspaket As Integer = -1
        Dim i As Integer

        ReDim pbl_ListVersionAp(UBound(pbl_VersionAp))
        Array.Copy(pbl_VersionAp, pbl_ListVersionAp, UBound(pbl_VersionAp))

        Try
            Index_Entwurf = ViewCombo.View_ComboboxEntwurf(ComboEntwurf, Array.FindIndex(pbl_Entwurf, Function(f) f.txtEntwurfstyp = pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp), pbl_Entwurf)
            For i = 0 To UBound(pbl_Entwurf)
                pbl_Entwurf(i).boolChecked = False
            Next

            Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                Index_Arbeitspaket = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
            End If
            Index_Arbeitspaket = ViewCombo.View_ComboboxArbeitspaket(comboAp, Index_Arbeitspaket, pbl_dtArbeitspaket)

            ListVersion.Items.Clear()
            ListVersion.Text = ""
            For i = 0 To UBound(pbl_ListVersionAp)
                If pbl_ListVersionAp(i).cbxFreigabe = False Then
                    pbl_ListVersionAp(i).boolChecked = False 'Check zurücksetzen
                    ListVersion.Items.Add(pbl_ListVersionAp(i).lngVersion)
                End If
            Next i

            ListGJ.Items.Clear()
            ListGJ.Text = ""
            For i = 0 To UBound(pbl_Gj)
                pbl_Gj(i).boolChecked = False 'Check zurücksetzen
                ListGJ.Items.Add(pbl_Gj(i).intGJ)
            Next i
        Catch
        End Try

    End Sub

    Public Sub ComboEntwurf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboEntwurf.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboEntwurf_SelectedIndexChanged")

        Erase pbl_ListVersionAp
        pbl_ListVersionAp = Get_VersionArbeitspaket(False, pbl_dtArbeitspaket.Rows(comboAp.SelectedIndex).Item("ID_AP"), pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)

        ListVersion.Items.Clear()
        ListVersion.Text = ""
        For i = 0 To UBound(pbl_ListVersionAp)
            If pbl_ListVersionAp(i).cbxFreigabe = False Then
                pbl_ListVersionAp(i).boolChecked = False 'Check zurücksetzen
                ListVersion.Items.Add(pbl_ListVersionAp(i).lngVersion)
            End If
        Next i
    End Sub

    Public Sub comboAp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles comboAp.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub comboAp_SelectedIndexChanged")

        Erase pbl_ListVersionAp
        pbl_ListVersionAp = Get_VersionArbeitspaket(False, pbl_dtArbeitspaket.Rows(comboAp.SelectedIndex).Item("ID_AP"), pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)

        ListVersion.Items.Clear()
        ListVersion.Text = ""
        For i = 0 To UBound(pbl_ListVersionAp)
            If pbl_ListVersionAp(i).cbxFreigabe = False Then
                pbl_ListVersionAp(i).boolChecked = False 'Check zurücksetzen
                ListVersion.Items.Add(pbl_ListVersionAp(i).lngVersion)
            End If
        Next i
    End Sub
#End Region

#Region "Aktionen"
    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Console.WriteLine("AKTION: Private Sub cmdSaveData_Click")

        'Prüfen ob eine aktive Verbindung zum Server existiert
        If con.State = ConnectionState.Closed Or boolConnected = False Then

            MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            Exit Sub
        End If

        If ComboEntwurf.SelectedIndex < 0 Or comboAp.SelectedIndex < 0 Then
            MsgBox("Bitte wählen Sie ein Arbeitspaket und einen Entwurfstyp aus", MsgBoxStyle.Critical, "Fehler")
            Exit Sub
        End If

        'Prüft ob Checkboxen gewählt wurden
        Try
            For Each indexCheckedVersion As Object In ListVersion.CheckedIndices
                pbl_ListVersionAp(indexCheckedVersion.ToString()).boolChecked = True
            Next
            For Each indexCheckedGJ As Object In ListGJ.CheckedIndices
                pbl_Gj(indexCheckedGJ.ToString()).boolChecked = True
            Next
            If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                MsgBox("Es können keine Daten auf 'Ist' kopiert werden. Die 'Ist' Daten kommen aus dem SAP.", MsgBoxStyle.Critical, "Fehler beim Kopieren")
                boolError = True
            Else
                pbl_Entwurf(ComboEntwurf.SelectedIndex).boolChecked = True
                boolError = False
            End If
            IDCopyArbeitspaket = pbl_dtArbeitspaket.Rows(comboAp.SelectedIndex).Item("ID_AP")
        Catch ex As Exception
            MsgBox(ex.Message.ToString, vbCritical, "Fehler")
        End Try

        Form_Edit.ToolStripStatusOperation.Text = "Daten erfolgreich kopiert!"
        Form_Edit.StatusStrip.Refresh()
        Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
        Form_Edit.TimerEditForm.Start()

        Me.Close()
    End Sub
#End Region

End Class