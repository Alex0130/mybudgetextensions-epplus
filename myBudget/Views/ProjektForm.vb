﻿Public Class ProjektForm
    Private Sub ProjektForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub ProjektForm_Load")

        Try
            If intArt_der_Editierung = 1 Then 'Änderung
                ViewData_Form_Projekt(0)
            ElseIf intArt_der_Editierung = 2 Then 'View

                Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & pbl_IdProjekt & "")
                If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                    Dim IndexProjekt As Integer = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                    ViewData_Form_Projekt(IndexProjekt)
                End If

            End If
        Catch ex As Exception
            MsgBox("ProjektForm_Load - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            Exit Sub 'Keine Daten zum laden
        End Try

        'Abhängig von der Art der Bearbeitung werden Felder befüllt oder nicht
        If intArt_der_Editierung = 0 Then 'Neuer Eintrag -> keine Daten zum AP vorhanden
            Form_Projekt.Text = "Erstellung Projekt"
            ComboProjekt.Enabled = False
            ComboProjekt.Visible = False
            lblProjekt.Visible = False
            txtProjekt.Enabled = True
            DateTimeProjektstart.Enabled = True
            txtProjektlaufzeit.Enabled = True
            cmdSaveData.Visible = True
        ElseIf intArt_der_Editierung = 1 Then 'Bearbeitung
            Form_Projekt.Text = "Änderung Projekt"
            ComboProjekt.Enabled = True
            ComboProjekt.Visible = True
            lblProjekt.Visible = True
            txtProjekt.Enabled = True
            DateTimeProjektstart.Enabled = True
            txtProjektlaufzeit.Enabled = True
            cmdSaveData.Visible = True
        Else 'View
            Form_Projekt.Text = "Anzeige Projekt"
            ComboProjekt.Enabled = False
            ComboProjekt.Visible = False
            lblProjekt.Visible = False
            txtProjekt.Enabled = False
            DateTimeProjektstart.Enabled = False
            txtProjektlaufzeit.Enabled = False
            cmdSaveData.Visible = False
        End If
    End Sub

    Function ViewData_Form_Projekt(Index_Projekt As Integer)
        Console.WriteLine("FUNC: Function ViewData_Form_Projekt")

        ComboProjekt.Text = ""
        ComboProjekt.Items.Clear()
        txtProjekt.Text = ""
        DateTimeProjektstart.Value = Now
        txtProjektlaufzeit.Text = ""

        If Index_Projekt < 0 Then Exit Function 'Kein Projekt --> keine View oder Eingabe möglich

        'Projekte müssen nicht geladen werden, da diese bei der Useranmeldung gezogen werden, basierend auf Berechtigungen
        Try
            Index_Projekt = ViewCombo.View_ComboboxProjekt(ComboProjekt, Index_Projekt, pbl_dtProjekt) 'Projekte anzeigen
            txtProjekt.Text = pbl_dtProjekt.Rows(Index_Projekt).Item("txtProjekt")
            DateTimeProjektstart.Value = pbl_dtProjekt.Rows(Index_Projekt).Item("datProjektstart")
            txtProjektlaufzeit.Text = pbl_dtProjekt.Rows(Index_Projekt).Item("intProjektlaufzeit")
        Catch ex As Exception
            MsgBox("ViewData_Form_Projekt - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Function

    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Console.WriteLine("AKTION: Private Sub cmdSaveData_Click")

        Dim i As Integer
        Dim Projekt_ID As Integer
        Dim intYear As Integer

        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            If txtProjekt.Text = "" Then
                MsgBox("Bitte geben Sie einen Projektnamen an", MsgBoxStyle.Critical, "Fehler")
                Exit Sub
            End If
            If txtProjektlaufzeit.Text = "" Or Not IsNumeric(txtProjektlaufzeit.Text) Then
                MsgBox("Bitte geben Sie eine Projektlaufzeit als ganze Zahl an", MsgBoxStyle.Critical, "Fehler")
                Exit Sub
            End If

            boolError = False

#Region "Neues Projekt anlegen"
            If intArt_der_Editierung = 0 Then 'NEUES PROJEKT ANLEGEN
                '1. Projekt anlegen
                '2. Authorisierung anlegen
                '3. GJ anlegen
                '4. EP anlegen
                '5. Projekte neu laden
                Dim NewId As Integer = -1
                Try
                    NewId = Add_Projekt(txtProjekt.Text, DateTimeProjektstart.Value, txtProjektlaufzeit.Text)
                Catch ex As Exception
                    boolError = True
                    MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                    Exit Sub 'Projekt Anlage fehlgeschlagen
                End Try
                If NewId >= 0 Then
                    Try 'Projekt Abhängigkeiten
                        Add_Authorization("ID_PROJEKT", NewId, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), "Write")
                        pbl_dtAuth = Get_tblAuthorization(True, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"))

                        intYear = Year(DateTimeProjektstart.Value)
                        For i = 1 To txtProjektlaufzeit.Text 'GJ anlegen
                            Add_GJ(intYear, NewId)
                            intYear = intYear + 1
                        Next

                        Add_Entwicklungspaket("none", "Initiale Anlage", NewId) 'EP anlegen
                        InitArbeitspaketAndVersion(NewId) 'AP und Version anlegen  !ab added
#Region "Projekte neu laden"
                        Dim sql As String, where As String
                        where = SqlWhereFieldEquals("ID_PROJEKT", pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)
                        sql = " where " & where
                        Get_tblProjektViaSql(True, "*", sql, True)
#End Region
                    Catch ex As Exception
                        boolError = True
                        MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                    End Try
                End If
#End Region
            Else 'XXX PROJEKT ÄNDERN

            End If
        Catch ex As Exception
            MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try

        Me.Close()
    End Sub

    Private Sub InitArbeitspaketAndVersion(idProjekt As Integer)    '!ab added
        Console.WriteLine("ACTION: Private Sub InitArbeitspaketAndVersion")

        Dim NewID_AP As String = Add_Arbeitspaket("Projektleitung", "", "", DateTimeProjektstart.Value, idProjekt, "", "EM")   'AP anlegen
        If NewID_AP >= 0 Then
            Try 'Abhängigkeiten von Arbeitspaket: Paket und AP Versionen
                Add_Paket("none", "Initiales Paket", NewID_AP)
                For i = 0 To UBound(pbl_Entwurf)
                    Add_VersionArbeitspaket(NewID_AP, 1, "Initiale Version", pbl_Entwurf(i).txtEntwurfstyp)     'VA anlegen
                Next
            Catch ex As Exception
                boolError = True
                MsgBox("InitArbeitsPaketAndVersion - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            End Try
        End If
    End Sub
End Class