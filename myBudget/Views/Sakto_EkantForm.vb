﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Deployment
Imports System.Reflection
Imports myBudget.UI

Public Class Sakto_EkantForm

    Public Sub Form_Load()
        Dim I As Long, lngRow As Long

        '1. Prüfen ob Sakto oder Ekant Daten geladen werden: Die Unterscheidung gilt für alles
        '2. DataGridView leeren
        '3. DataGridView formatieren
        '4. Daten in DataGridView anzeige

        '1.
        Dim listSakto As System.Data.DataTable
        Dim listEkant As System.Data.DataTable

        If intForm_Sakto_Ekant = 0 Then
            listSakto = Get_tblSachkonto(False)
        ElseIf intForm_Sakto_Ekant = 1 Then
            listEkant = Get_tblEkant(False)
        End If

        '2.
        iGridS.Cols.Clear()
        iGridS.Rows.Clear()

        '3.
        If intForm_Sakto_Ekant = 0 Then 'Sachkonto Tabelle
            iGridS.FillWithData(listSakto, False)

#Region "Columns"
            iGridS.Cols("qryKostenart").Text = "Kostenart"
            iGridS.Cols("qryKostenart").Order = 0

            iGridS.Cols("Sakto").Text = "Sachkonto"
            iGridS.Cols("Sakto").Order = 1

            iGridS.Cols("txtSachkonto").Text = "Sachkonto Beschreibung"
            iGridS.Cols("txtSachkonto").Order = 2

            iGridS.Cols("FEHLER").Visible = False
#End Region

        ElseIf intForm_Sakto_Ekant = 1 Then 'Ekant Liste
            iGridS.FillWithData(listEkant, False)

#Region "Columns"
            iGridS.Cols("Std_Lst").Text = "Stunden/Leistungsart"
            iGridS.Cols("Std_Lst").Order = 0

            iGridS.Cols("Abteilung").Text = "Abteilung"
            iGridS.Cols("Abteilung").Order = 1

            iGridS.Cols("Kostenstelle").Text = "Kostenstelle"
            iGridS.Cols("Kostenstelle").Order = 2

            iGridS.Cols("Kst_Schlüssel").Text = "Schlüssel der Kostenstelle"
            iGridS.Cols("Kst_Schlüssel").Order = 3

            iGridS.Cols("Bezeichnung").Text = "Bezeichnung"
            iGridS.Cols("Bezeichnung").Order = 4

            iGridS.Cols("Std_Satz").Text = "Stundensatz"
            iGridS.Cols("Std_Satz").Order = 5
#End Region
        End If

        iGridS.Header.Font = New Font(iGridS.Font, FontStyle.Bold)

        iGridS.Cols.AutoWidth()
        iGridS.Rows.AutoHeight()
    End Sub

    Private Sub Sakto_EkantForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If ApplicationExit = False Then
            e.Cancel = True
            Me.Hide() 'Form wird nicht geschlossen sondern nur unsichtbar. DAmit kann sie ein zweites mal geöffnet werden
        End If
    End Sub

    Public Sub Sakto_EkantForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        If Not intForm_Sakto_Ekant = -1 Then
            Form_Load()
            intForm_Sakto_Ekant = -1
            AddHandler MyBase.Activated, AddressOf Sakto_EkantForm_Activated
        End If
    End Sub

    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        IGPrintManager1.PrintPreview()
    End Sub

    Public Sub IGAutoFilterManager1_FilterApplied(sender As Object, e As TenTec.Windows.iGridLib.Filtering.iGFilterAppliedEventArgs) Handles IGAutoFilterManager1.FilterApplied
        RemoveHandler MyBase.Activated, AddressOf Sakto_EkantForm_Activated
    End Sub
End Class