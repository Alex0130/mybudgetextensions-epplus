﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectConnectionForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectConnectionForm))
        Me.cmdSave = New System.Windows.Forms.PictureBox()
        Me.ComboDatenbank = New System.Windows.Forms.ComboBox()
        CType(Me.cmdSave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSave
        '
        Me.cmdSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.Location = New System.Drawing.Point(204, 39)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(35, 35)
        Me.cmdSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmdSave.TabIndex = 38
        Me.cmdSave.TabStop = False
        '
        'ComboDatenbank
        '
        Me.ComboDatenbank.BackColor = System.Drawing.Color.DarkRed
        Me.ComboDatenbank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboDatenbank.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboDatenbank.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboDatenbank.ForeColor = System.Drawing.Color.White
        Me.ComboDatenbank.FormattingEnabled = True
        Me.ComboDatenbank.Location = New System.Drawing.Point(12, 12)
        Me.ComboDatenbank.Name = "ComboDatenbank"
        Me.ComboDatenbank.Size = New System.Drawing.Size(227, 21)
        Me.ComboDatenbank.TabIndex = 39
        '
        'SelectConnectionForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(251, 85)
        Me.Controls.Add(Me.ComboDatenbank)
        Me.Controls.Add(Me.cmdSave)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SelectConnectionForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datenbank"
        CType(Me.cmdSave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cmdSave As System.Windows.Forms.PictureBox
    Friend WithEvents ComboDatenbank As System.Windows.Forms.ComboBox
End Class
