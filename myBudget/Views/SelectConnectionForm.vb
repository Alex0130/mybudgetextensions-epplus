﻿Public Class SelectConnectionForm
    Private Sub SelectConnectionForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Long
        If Not IsNothing(listConnections) Then
            For i = LBound(listConnections) To UBound(listConnections)
                ComboDatenbank.Items.Add(listConnections(i).System)
            Next
            ComboDatenbank.SelectedIndex = 0
        End If
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        pbl_SelectedConnection = ComboDatenbank.SelectedIndex
        Me.Close()
    End Sub

End Class