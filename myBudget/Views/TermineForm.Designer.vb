﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TermineForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TermineForm))
        Me.iGridT = New TenTec.Windows.iGridLib.iGrid()
        Me.IGrid1DefaultCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.IGrid1DefaultColHdrStyle = New TenTec.Windows.iGridLib.iGColHdrStyle(True)
        Me.IGrid1RowTextColCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.CommandGo = New System.Windows.Forms.PictureBox()
        Me.DateFrom = New System.Windows.Forms.DateTimePicker()
        Me.DateTo = New System.Windows.Forms.DateTimePicker()
        Me.iGridFilter = New TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LabelStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Line1 = New System.Windows.Forms.PictureBox()
        Me.SaveData = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gridTv = New System.Windows.Forms.DataGridView()
        Me.PSP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExcelExport = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.iGridT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandGo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.Line1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SaveData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridTv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExcelExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'iGridT
        '
        Me.iGridT.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.iGridT.DefaultCol.CellStyle = Me.IGrid1DefaultCellStyle
        Me.iGridT.DefaultCol.ColHdrStyle = Me.IGrid1DefaultColHdrStyle
        Me.iGridT.GroupBox.Visible = True
        Me.iGridT.Location = New System.Drawing.Point(12, 169)
        Me.iGridT.Name = "iGridT"
        Me.iGridT.RowHeader.Visible = True
        Me.iGridT.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.MultipleRows
        Me.iGridT.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.MultiExtended
        Me.iGridT.Size = New System.Drawing.Size(776, 269)
        Me.iGridT.TabIndex = 0
        Me.iGridT.VScrollBar.CustomButtons.AddRange(New TenTec.Windows.iGridLib.iGScrollBarCustomButton() {New TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Near, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, "Collapse", True, Nothing), New TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Near, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, "Expand", True, Nothing)})
        Me.iGridT.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always
        '
        'CommandGo
        '
        Me.CommandGo.Image = CType(resources.GetObject("CommandGo.Image"), System.Drawing.Image)
        Me.CommandGo.Location = New System.Drawing.Point(502, 25)
        Me.CommandGo.Name = "CommandGo"
        Me.CommandGo.Size = New System.Drawing.Size(65, 68)
        Me.CommandGo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CommandGo.TabIndex = 1
        Me.CommandGo.TabStop = False
        '
        'DateFrom
        '
        Me.DateFrom.Location = New System.Drawing.Point(284, 25)
        Me.DateFrom.Name = "DateFrom"
        Me.DateFrom.Size = New System.Drawing.Size(200, 20)
        Me.DateFrom.TabIndex = 3
        '
        'DateTo
        '
        Me.DateTo.Location = New System.Drawing.Point(284, 76)
        Me.DateTo.Name = "DateTo"
        Me.DateTo.Size = New System.Drawing.Size(200, 20)
        Me.DateTo.TabIndex = 4
        '
        'iGridFilter
        '
        Me.iGridFilter.Grid = Me.iGridT
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LabelStatus})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 428)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(800, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LabelStatus
        '
        Me.LabelStatus.Name = "LabelStatus"
        Me.LabelStatus.Size = New System.Drawing.Size(12, 17)
        Me.LabelStatus.Text = "-"
        '
        'Line1
        '
        Me.Line1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Line1.BackColor = System.Drawing.Color.DarkRed
        Me.Line1.Location = New System.Drawing.Point(0, 120)
        Me.Line1.Name = "Line1"
        Me.Line1.Size = New System.Drawing.Size(850, 2)
        Me.Line1.TabIndex = 23
        Me.Line1.TabStop = False
        '
        'SaveData
        '
        Me.SaveData.Image = CType(resources.GetObject("SaveData.Image"), System.Drawing.Image)
        Me.SaveData.Location = New System.Drawing.Point(12, 128)
        Me.SaveData.Name = "SaveData"
        Me.SaveData.Size = New System.Drawing.Size(35, 35)
        Me.SaveData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SaveData.TabIndex = 38
        Me.SaveData.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(245, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 16)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Liefertermin"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(245, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 16)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "von"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(249, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 16)
        Me.Label3.TabIndex = 41
        Me.Label3.Text = "bis"
        '
        'gridTv
        '
        Me.gridTv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridTv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PSP})
        Me.gridTv.Location = New System.Drawing.Point(12, 6)
        Me.gridTv.Margin = New System.Windows.Forms.Padding(2)
        Me.gridTv.Name = "gridTv"
        Me.gridTv.RowTemplate.Height = 33
        Me.gridTv.Size = New System.Drawing.Size(184, 109)
        Me.gridTv.TabIndex = 42
        '
        'PSP
        '
        Me.PSP.HeaderText = "PSP-Element(e)"
        Me.PSP.Name = "PSP"
        '
        'ExcelExport
        '
        Me.ExcelExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ExcelExport.Image = CType(resources.GetObject("ExcelExport.Image"), System.Drawing.Image)
        Me.ExcelExport.Location = New System.Drawing.Point(753, 128)
        Me.ExcelExport.Name = "ExcelExport"
        Me.ExcelExport.Size = New System.Drawing.Size(35, 35)
        Me.ExcelExport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ExcelExport.TabIndex = 63
        Me.ExcelExport.TabStop = False
        '
        'Timer1
        '
        '
        'TermineForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.ExcelExport)
        Me.Controls.Add(Me.gridTv)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SaveData)
        Me.Controls.Add(Me.Line1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.DateTo)
        Me.Controls.Add(Me.DateFrom)
        Me.Controls.Add(Me.CommandGo)
        Me.Controls.Add(Me.iGridT)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "TermineForm"
        Me.Text = "Terminverfolgung"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.iGridT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandGo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.Line1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SaveData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridTv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExcelExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents iGridT As TenTec.Windows.iGridLib.iGrid
    Friend WithEvents IGrid1DefaultCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents IGrid1DefaultColHdrStyle As TenTec.Windows.iGridLib.iGColHdrStyle
    Friend WithEvents IGrid1RowTextColCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents CommandGo As PictureBox
    Friend WithEvents DateFrom As DateTimePicker
    Friend WithEvents DateTo As DateTimePicker
    Friend WithEvents iGridFilter As TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LabelStatus As ToolStripStatusLabel
    Friend WithEvents Line1 As PictureBox
    Friend WithEvents SaveData As PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gridTv As DataGridView
    Friend WithEvents PSP As DataGridViewTextBoxColumn
    Friend WithEvents ExcelExport As PictureBox
    Friend WithEvents Timer1 As Timer
End Class
