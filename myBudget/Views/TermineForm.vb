﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Deployment
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports myBudget.UI
Imports Microsoft.Office.Interop
Imports System.Diagnostics
Imports TenTec.Windows.iGridLib

Public Class TermineForm
#Region "Aktionen"
    Private Sub CommandGo_Click(sender As Object, e As EventArgs) Handles CommandGo.Click
        Console.WriteLine("AKTION: Private Sub CommandGo_Click")

        Try
            LabelStatus.Text = "Daten werden heruntergeladen..."
            RemoveHandler iGridFilter.FilterApplied, AddressOf iGridFilter_FilterApplied

            Dim i As Integer
            Dim row As Integer = -1
            Dim listgj(0) As String
            If Not gridTv.Rows.Count = 0 Then
                For i = 0 To gridTv.Rows.Count - 1
                    If Not String.IsNullOrEmpty(gridTv.Rows(i).Cells("PSP").Value) Then
                        row = row + 1
                        ReDim Preserve listgj(row)
                        listgj(row) = gridTv.Rows(i).Cells("PSP").Value
                    End If
                Next
            End If

            Dim VonDate As DateTime = DateFrom.Value.ToShortDateString
            Dim BisDate As DateTime = DateTo.Value.ToShortDateString
            Dim VON As String = VonDate.ToString("yyyyMMdd")    '!ab mod.
            Dim BIS As String = BisDate.ToString("yyyyMMdd")    '!ab mod.

            Dim dt As System.Data.DataTable = Get_DatesAndComments(VON, BIS, listgj)

            iGridT.Cols.Clear()
            iGridT.Rows.Clear()

            iGridT.FillWithData(dt, False)

            iGridLayout()

            iGridHighlight()

            LabelStatus.Text = "Daten erfolgreich heruntergeladen."
            Timer1.Interval = 5000 '1000 = 1 sec
            Timer1.Start()
        Catch ex As Exception
            MsgBox("CommandGo_Click: " & ex.Message.ToString)
        End Try

        AddHandler iGridFilter.FilterApplied, AddressOf iGridFilter_FilterApplied
    End Sub

    Private Sub SaveData_Click(sender As Object, e As EventArgs) Handles SaveData.Click
        Console.WriteLine("AKTION: Private Sub SaveData_Click")

        RemoveHandler iGridFilter.FilterApplied, AddressOf iGridFilter_FilterApplied
        iGridSave()
        AddHandler iGridFilter.FilterApplied, AddressOf iGridFilter_FilterApplied
    End Sub

    Private Sub ExcelExport_Click(sender As Object, e As EventArgs) Handles ExcelExport.Click
        Try
            LabelStatus.Text = "Daten werden in Excel exportiert..."
            StatusStrip1.Refresh()

            iGridToExcel(Form_Termine.iGridT)
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        LabelStatus.Text = "Daten erfolgreich exportiert"
        StatusStrip1.Refresh()
        Timer1.Interval = 5000 '1000 = 1 sec
        Timer1.Start()
    End Sub
#End Region

#Region "Events"
    Private Sub TermineForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub TermineForm_Load")

        LabelStatus.Text = ""
        Dim i As Integer
        Dim row As Integer = -1
        gridTv.Rows.Clear()

        If pbl_dtArbeitspaket.Rows.Count > 0 Then
            For i = 0 To pbl_dtArbeitspaket.Rows.Count - 1
                gridTv.Rows.Add()
                row = row + 1
                gridTv.Rows(row).Cells("PSP").Value = pbl_dtArbeitspaket.Rows(i).Item("txtPSP_Element")
            Next
        End If

        DateFrom.Value = New Date(Year(Now), 1, 1)
        DateTo.Value = New Date(Year(Now), 12, 31)
    End Sub

    Private Sub FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Console.WriteLine("EVENT: Private Sub FormClosing")

        If ApplicationExit = False Then
            e.Cancel = True
            Me.Hide() 'Form wird nicht geschlossen sondern nur unsichtbar. DAmit kann sie ein zweites mal geöffnet werden
        End If
    End Sub

    Private Sub FormActivated(sender As Object, e As EventArgs) Handles MyBase.Activated
        gridTv.ColumnHeadersDefaultCellStyle.Font = New Font(iGridT.Font, FontStyle.Bold)
        Console.WriteLine("EVENT: Private Sub FormActivated")

    End Sub

    Private Sub iGridFilter_FilterApplied(sender As Object, e As TenTec.Windows.iGridLib.Filtering.iGFilterAppliedEventArgs) Handles iGridFilter.FilterApplied
        Console.WriteLine("EVENT: Private Sub iGridFilter_FilterApplied")

        Dim i As Integer, count As Integer = 1
        Try
            If iGridT.Rows.Count > 0 Then
                For i = 0 To iGridT.Rows.Count - 1
                    If iGridT.Rows(i).Visible = True Then
                        iGridT.Rows(i).Cells("Nr").Value = count
                        count = count + 1
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        RemoveHandler MyBase.Activated, AddressOf FormActivated

        AddHandler MyBase.Activated, AddressOf FormActivated
    End Sub

    Private Sub iGridT_AfterContentsSorted(sender As Object, e As EventArgs) Handles iGridT.AfterContentsSorted
        Console.WriteLine("EVENT: Private Sub iGridT_AfterContentsSorted")

        Dim i As Integer, count As Integer = 1
        Try
            If iGridT.Rows.Count > 0 Then
                For i = 0 To iGridT.Rows.Count - 1
                    If iGridT.Rows(i).Visible = True Then
                        iGridT.Rows(i).Cells("Nr").Value = count
                        count = count + 1
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub iGridT_AfterCommitEdit(sender As Object, e As TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs) Handles iGridT.AfterCommitEdit
        Console.WriteLine("EVENT: Private Sub iGridT_AfterCommitEdit")

        Try
            If Not CheckCellValueChanged2 = iGridT.Rows(e.RowIndex).Cells(e.ColIndex).Value.ToString Then 'Datensatz wurde geändert
                'boolDataChanged = True 'Es haben sich Daten geändert

#Region "Define Action"
                If iGridT.Rows(e.RowIndex).Cells("ID").Value = 0 Then
                    iGridT.Rows(e.RowIndex).Cells("Action").Value = "N" 'New
                ElseIf iGridT.Rows(e.RowIndex).Cells("Action").Value = "D" Then
                    iGridT.Rows(e.RowIndex).Cells("Action").Value = "D" 'Delete
                Else
                    iGridT.Rows(e.RowIndex).Cells("Action").Value = "C" 'Change
                End If
#End Region
            End If
        Catch ex As Exception
            MsgBox("igridtT_AfterCommitEdit : " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub iGridT_BeforeCommitEdit(sender As Object, e As TenTec.Windows.iGridLib.iGBeforeCommitEditEventArgs) Handles iGridT.BeforeCommitEdit
        Console.WriteLine("EVENT: Private Sub iGridT_BeforeCommitEdit")

        Try
            If Not String.IsNullOrEmpty(iGridT.Rows(e.RowIndex).Cells(e.ColIndex).Value) Then
                CheckCellValueChanged2 = iGridT.Rows(e.RowIndex).Cells(e.ColIndex).Value.ToString
            Else
                CheckCellValueChanged2 = ""
            End If
        Catch ex As Exception
            MsgBox("iGridT_BeforeCommitEdit : " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub iGridT_AfterContentsGrouped(sender As Object, e As EventArgs) Handles iGridT.AfterContentsGrouped
        Console.WriteLine("EVENT: Private Sub iGridT_AfterContentsGrouped")

        iGridT.Rows.CollapseAll()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Console.WriteLine("< Private Sub Timer1_Tick")

        LabelStatus.Text = ""
        Timer1.Stop()
    End Sub
#End Region

#Region "Methoden"
    Sub iGridSave()
        Console.WriteLine("FUNC: Sub iGridSave")

        Dim i As Integer
        If iGridT.Rows.Count = 0 Then Exit Sub

        For i = 0 To iGridT.Rows.Count - 1
            Dim boolStatus As Boolean = False
            If iGridT.Rows(i).Cells("Action").Value = "N" Then
                '1. Prüfen ob es mittlerweile schon eine ID gibt
                '2. Wenn ja: Update, Wenn nein: Eintrag erstellen
                Dim ID = Get_ID(iGridT.Rows(i).Cells("SapId").Value)
                If ID = 0 Then
                    boolStatus = Add_CommentRow(iGridT.Rows(i).Cells("Kommentar1").Value, iGridT.Rows(i).Cells("Kommentar2").Value, iGridT.Rows(i).Cells("Kommentar3").Value, iGridT.Rows(i).Cells("SapId").Value, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), iGridT.Rows(i).Cells("PSP").Value)
                Else
                    boolStatus = Update_CommentRow(iGridT.Rows(i).Cells("Kommentar1").Value, iGridT.Rows(i).Cells("Kommentar2").Value, iGridT.Rows(i).Cells("Kommentar3").Value, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), iGridT.Rows(i).Cells("PSP").Value, ID)
                End If

            ElseIf iGridT.Rows(i).Cells("Action").Value = "C" Then
                boolStatus = Update_CommentRow(iGridT.Rows(i).Cells("Kommentar1").Value, iGridT.Rows(i).Cells("Kommentar2").Value, iGridT.Rows(i).Cells("Kommentar3").Value, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), iGridT.Rows(i).Cells("PSP").Value, iGridT.Rows(i).Cells("ID").Value)
            End If
            If boolStatus = True Then
                iGridT.Rows(i).Cells("Action").Value = ""
                LabelStatus.Text = "Zeile " & i & " erfolgreich gespeichert."
                Timer1.Interval = 5000 '1000 = 1 sec
                Timer1.Start()
            End If
        Next
    End Sub

    Sub iGridLayout()
        Console.WriteLine("FUNC: Sub iGridLayout")

        Try
            iGridT.BeginUpdate()

            iGridT.Header.Font = New Font(iGridT.Font, FontStyle.Bold)

#Region "ReadOnly Cols"
            For i = 0 To iGridT.Cols.Count - 1
                iGridT.Cols(i).CellStyle.ReadOnly = False
            Next
            iGridT.Cols("Kommentar1").CellStyle.ReadOnly = True
            iGridT.Cols("Kommentar2").CellStyle.ReadOnly = True
            iGridT.Cols("Kommentar3").CellStyle.ReadOnly = True
#End Region

#Region "Col visibility"
            If Not InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 Then
                iGridT.Cols("ID").Visible = False
                iGridT.Cols("Action").Visible = False
                iGridT.Cols("Auftrag").Visible = False
                iGridT.Cols("Kostenstelle").Visible = False
                iGridT.Cols("Preiseinheit").Visible = False
                iGridT.Cols("Änderungsdatum").Visible = False
                iGridT.Cols("Endrechnung").Visible = False
                iGridT.Cols("Bedarfsnummer").Visible = False
            Else
                iGridT.Cols("ID").Visible = True
                iGridT.Cols("Action").Visible = True
                iGridT.Cols("Auftrag").Visible = True
                iGridT.Cols("Auftrag").Visible = True
                iGridT.Cols("Preiseinheit").Visible = True
                iGridT.Cols("Änderungsdatum").Visible = True
                iGridT.Cols("Endrechnung").Visible = True
                iGridT.Cols("Bedarfsnummer").Visible = True
            End If

            iGridT.Cols("Änderungsdatum").CellStyle.FormatString = "{0:d}"

#End Region

#Region "Col order"
            iGridT.Cols("Nr").Order = 0
            iGridT.Cols("Nr").Text = "Nr."

            iGridT.Cols("ZeitBisLieferung").Order = 1
            iGridT.Cols("ZeitBisLieferung").Text = "Zeit bis Lieferung"

            iGridT.Cols("Bestellung").Order = 2

            iGridT.Cols("BestellPosition").Order = 3
            iGridT.Cols("BestellPosition").Text = "Best. Pos."

            iGridT.Cols("Materialnummer").Order = 4

            iGridT.Cols("Materialkurztext").Order = 5

            iGridT.Cols("Menge").Order = 6

            iGridT.Cols("Mengeneinheit").Order = 7
            iGridT.Cols("Mengeneinheit").Text = "Einheit"

            iGridT.Cols("Liefermenge").Order = 8
            iGridT.Cols("Liefermenge").Text = "gelieferte Menge"

            iGridT.Cols("LieferantenNummer").Order = 9
            iGridT.Cols("LieferantenNummer").Text = "Nr. Lieferant"

            iGridT.Cols("Lieferant").Order = 10

            iGridT.Cols("Lieferdatum").Order = 11
            iGridT.Cols("Lieferdatum").CellStyle.FormatString = "{0:d}"

            iGridT.Cols("Preis").Order = 12
            iGridT.Cols("Preis").Text = "Preis/St."

            iGridT.Cols("Währung").Order = 13

            iGridT.Cols("PreisGesamt").Order = 14

            iGridT.Cols("PSP").Order = 15

            iGridT.Cols("Kommentar1").Order = 16

            iGridT.Cols("Kommentar2").Order = 17

            iGridT.Cols("Kommentar3").Order = 18

            iGridT.Cols("Lieferzeit").Order = 19

            iGridT.Cols("Stat_Lieferdatum").Order = 20
            iGridT.Cols("Stat_Lieferdatum").Text = "Banf. Lieferdatum"
            iGridT.Cols("Stat_Lieferdatum").CellStyle.FormatString = "{0:d}"

            iGridT.Cols("LieferterminAbweichung").Order = 21
            iGridT.Cols("LieferterminAbweichung").Text = "Abweichung Liefertermin"

            iGridT.Cols("Sachkonto").Order = 22

            iGridT.Cols("Lieferkennzeichen").Order = 23

            iGridT.Cols("WE_Haken").Order = 24
            iGridT.Cols("WE_Haken").Text = "WE j/n"

            iGridT.Cols("RE_Haken").Order = 25
            iGridT.Cols("RE_Haken").Text = "RE j/n"
#End Region

            iGridT.Rows.AutoHeight()
            iGridT.Cols.AutoWidth()

            iGridT.EndUpdate()
        Catch ex As Exception
            MsgBox("iGridLayout: " & ex.Message.ToString)
        End Try

    End Sub

    Sub iGridHighlight()
        Console.WriteLine("FUNC: Sub iGridHighlight")

        If iGridT.Rows.Count = 0 Then Exit Sub

        Try
            Dim i As Integer
            For i = 0 To iGridT.Rows.Count - 1
                If iGridT.Rows(i).Cells("LieferterminAbweichung").Value <= 0 Then
                    iGridT.Rows(i).Cells("LieferterminAbweichung").BackColor = Color.DarkGreen
                ElseIf iGridT.Rows(i).Cells("LieferterminAbweichung").Value > 0 AndAlso iGridT.Rows(i).Cells("LieferterminAbweichung").Value < 5 Then
                    iGridT.Rows(i).Cells("LieferterminAbweichung").BackColor = Color.Orange
                Else
                    iGridT.Rows(i).Cells("LieferterminAbweichung").BackColor = Color.LightPink
                End If

                If iGridT.Rows(i).Cells("ZeitBisLieferung").Value <= 0 Then
                    iGridT.Rows(i).Cells("ZeitBisLieferung").BackColor = Color.LightPink
                ElseIf iGridT.Rows(i).Cells("ZeitBisLieferung").Value > 0 AndAlso iGridT.Rows(i).Cells("ZeitBisLieferung").Value < 3 Then
                    iGridT.Rows(i).Cells("ZeitBisLieferung").BackColor = Color.Orange
                ElseIf iGridT.Rows(i).Cells("ZeitBisLieferung").Value >= 3 AndAlso iGridT.Rows(i).Cells("ZeitBisLieferung").Value < 10 Then
                    iGridT.Rows(i).Cells("ZeitBisLieferung").BackColor = Color.LightGreen
                Else
                    iGridT.Rows(i).Cells("ZeitBisLieferung").BackColor = Color.Green
                End If
            Next
        Catch ex As Exception
            MsgBox("iGridHighlight: " & ex.Message.ToString)
        End Try
    End Sub
#End Region
End Class