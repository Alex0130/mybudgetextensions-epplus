﻿Public Class UserEmailForm
#Region "Methoden"
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        'empty string löscht die Email-Adresse und löscht alle Notifications
        lblWrongFormat.Visible = False
        SetCurrentUserEmailAddress(Nothing)
        RemoveAllUserSubscriptions()
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If CheckAddressFormat(txtEmailAddr.Text) Then
            lblWrongFormat.Visible = False
            SetCurrentUserEmailAddress(txtEmailAddr.Text)
            Me.Close()
        Else
            lblWrongFormat.Visible = True
        End If
    End Sub
#End Region

#Region "Events"
    Private Sub UserEmailForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If ApplicationExit = False Then
            e.Cancel = True
            Me.Hide() 'Form wird nicht geschlossen sondern nur unsichtbar. Damit kann sie ein zweites mal geöffnet werden
        End If
    End Sub
#End Region

End Class