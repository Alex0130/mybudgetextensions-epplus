﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VersionForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VersionForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtErläuterung = New System.Windows.Forms.TextBox()
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.ComboVersion = New System.Windows.Forms.ComboBox()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.cbxCopyData = New System.Windows.Forms.CheckBox()
        Me.cbxFreezeVersion = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(9, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Erläuterung"
        '
        'txtErläuterung
        '
        Me.txtErläuterung.Location = New System.Drawing.Point(12, 88)
        Me.txtErläuterung.Multiline = True
        Me.txtErläuterung.Name = "txtErläuterung"
        Me.txtErläuterung.Size = New System.Drawing.Size(288, 149)
        Me.txtErläuterung.TabIndex = 1
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.DarkRed
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(200, 243)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 1
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'ComboVersion
        '
        Me.ComboVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboVersion.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboVersion.FormattingEnabled = True
        Me.ComboVersion.Location = New System.Drawing.Point(12, 27)
        Me.ComboVersion.Name = "ComboVersion"
        Me.ComboVersion.Size = New System.Drawing.Size(288, 24)
        Me.ComboVersion.TabIndex = 0
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.ForeColor = System.Drawing.Color.DarkRed
        Me.lblVersion.Location = New System.Drawing.Point(9, 8)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(61, 16)
        Me.lblVersion.TabIndex = 32
        Me.lblVersion.Text = "Version"
        '
        'cbxCopyData
        '
        Me.cbxCopyData.AutoSize = True
        Me.cbxCopyData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCopyData.ForeColor = System.Drawing.Color.DarkRed
        Me.cbxCopyData.Location = New System.Drawing.Point(12, 251)
        Me.cbxCopyData.Name = "cbxCopyData"
        Me.cbxCopyData.Size = New System.Drawing.Size(157, 20)
        Me.cbxCopyData.TabIndex = 33
        Me.cbxCopyData.Text = "Daten übernehmen"
        Me.cbxCopyData.UseVisualStyleBackColor = True
        '
        'cbxFreezeVersion
        '
        Me.cbxFreezeVersion.AutoSize = True
        Me.cbxFreezeVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFreezeVersion.ForeColor = System.Drawing.Color.DarkRed
        Me.cbxFreezeVersion.Location = New System.Drawing.Point(12, 277)
        Me.cbxFreezeVersion.Name = "cbxFreezeVersion"
        Me.cbxFreezeVersion.Size = New System.Drawing.Size(182, 20)
        Me.cbxFreezeVersion.TabIndex = 34
        Me.cbxFreezeVersion.Text = "Alte Version schließen"
        Me.cbxFreezeVersion.UseVisualStyleBackColor = True
        '
        'VersionForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(312, 305)
        Me.Controls.Add(Me.cbxFreezeVersion)
        Me.Controls.Add(Me.cbxCopyData)
        Me.Controls.Add(Me.ComboVersion)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Controls.Add(Me.txtErläuterung)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "VersionForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Version"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtErläuterung As System.Windows.Forms.TextBox
    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
    Friend WithEvents ComboVersion As System.Windows.Forms.ComboBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents cbxCopyData As System.Windows.Forms.CheckBox
    Friend WithEvents cbxFreezeVersion As System.Windows.Forms.CheckBox
End Class
